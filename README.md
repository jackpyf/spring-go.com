# spring-go

#### 介绍
基于Go 语言编写适合Java程序员风格的代码框架，希望兄弟们能够加入进来，让Go语言开发更加简单，如果有问题也欢迎大家提出来，希望Go语言大拿多指点。

V0.1 ：
go-mvc框架是模拟java里面简单的mvc模式，由controller，service，dao三层构成。对不起，里面基本还没有任何注释，后期我会慢慢加上并优化代码。目前已经实现基本的mvc模式，可以通过autowized来注入相应的bean。支持拦截器来授权拦截，目前已经满足一个小型的api接口服务需求了。

V0.2
go-mvc 改名成 lig（Let's go）我女儿最喜欢听的歌曲，随他去的意思，以及对它进行了一些改动，看着以前不爽的东西都改掉了，可能下一个版本还会改动很多，哈哈。
A）lig提供了redis和kafka的支持，kafka用到了sarama这个client，个人有非常小的修改才可以让它完美注入，否则由于包名的权限问题导致注入丢失变量，后期我想重写这个client，因为这个client功能有限。
B）新增了一个tcp的框架，取名为lic（Let's come），这个希望这个框架随你怎么来都能够吃的下你的连接请求。寓意哈。
C）完全使用vendor模式了，因为go语言有的库变化太快，而且还有bug需要你去改，go的版本管理还是没有Java那么好，个人还是喜欢vendor
依旧没有注释，相信牛逼的你肯定能够看得懂，Go语言里面的权限还是挺有意思的，这也是为什么好多人把代码写在一个文件夹里面。

#### 软件架构
软件架构说明
待补充

#### 安装教程

1. vendor模式，直接把需要用到的包放到这里，推荐不要引入太多的包，在Go 1.6之前，你需要手动的设置环境变量GO15VENDOREXPERIMENT=1才可以使Go找到Vendor目录
2. 由于国内网络问题，请不要使用go mod模式，

#### 使用说明

V0.1
里面提供了两个例子，大致雷同。

1. app:=context.NewMVC获得整个MVC执行器
2. 创建Xorm数据库操作引擎，然后通过app.UseDao(engine)注入到app
3. app.InstallArr安装Controller，Service，Dao一条线的实例。
4. app.Interceptors安装拦截器，可以装最多是个，按照顺序执行，拦截器返回false,则httpcode是401，且返回内容显示到页面
5. app.ListenAndServe提供服务

V0.2 

lig：
里面提供了四个例子，大致雷同。
1. app:=context.NewMVC获得整个MVC执行器
2. 创建Xorm数据库操作引擎，然后通过app.UseEngine(engine)注入到app
3. app.Install安装Controller，Service，Dao等实例，提供support函数
4. app.Interceptors安装拦截器，可以装最多是个，按照顺序执行，拦截器返回false,则httpcode是401，且返回内容显示到页面
5. app.BuildResources扫描包
6. app.Runback这个是提供后台程序执行的，跟Java里面类似，就是即注入了对象，又可以后台常驻，主要为了kafka等订阅消息用
7. app.Publish提供端口服务

lic：
里面提供了一个例子，主要有一个server和两个client，大致已经实现了pack和unpack的动作，而且能够很友好的工作了。下一次会引入连接重连。


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)