package log4g

import (
	"errors"
	"os"
	"runtime"
	"sync"
	"time"
)

type consoleLogger struct {
	sync.Mutex
	Level    string `json:"level"`
	Colorful bool   `json:"color"`
	LogLevel int
}

type brush func(string) string

func newBrush(color string) brush {
	pre := "\033["
	reset := "\033[0m"
	return func(text string) string {
		return pre + color + "m" + text + reset
	}
}

// 前景 背景 颜色
// ---------------------------------------
// 30  40  黑色
// 31  41  红色
// 32  42  绿色
// 33  43  黄色
// 34  44  蓝色
// 35  45  紫红色
// 36  46  青蓝色
// 37  47  白色
//
// 代码 意义
// -------------------------
//  0  终端默认设置
//  1  高亮显示
//  4  使用下划线
//  5  闪烁
//  7  反白显示
//  8  不可见

//鉴于终端的通常使用习惯，一般白色和黑色字体是不可行的,所以30,37不可用，
var colors = []brush{
	newBrush("1;31"), // Critical           蓝色
	newBrush("0;31"), // Error              红色
	newBrush("1;33"), // Warn               黄色
	newBrush("0;34"), // Informational      蓝色
	newBrush("0;33"), // Debug              棕色
	newBrush("0;32"), // Trace              绿色
}

func (c *consoleLogger) InitConfig(config interface{}) error {
	if cc, ok := config.(*consoleLogger); ok {
		if runtime.GOOS == "windows" {
			c.Colorful = false
		}
		if l, ok := LevelMap[cc.Level]; ok {
			c.LogLevel = l
		}
		return nil
	} else {
		err := errors.New("consoleLogger initialized failed!")
		return err
	}
}

func (c *consoleLogger) LogWrite(when time.Time, msgText interface{}, level int) error {
	if level > c.LogLevel {
		return nil
	}
	msg, ok := msgText.(string)
	if !ok {
		return nil
	}
	if c.Colorful {
		msg = colors[level](msg)
	}
	c.printlnConsole(when, msg)
	return nil
}

func (c *consoleLogger) printlnConsole(when time.Time, msg string) {
	c.Lock()
	defer c.Unlock()
	os.Stdout.Write(append([]byte(msg), '\n'))
}

func (c *consoleLogger) Destroy() {

}

/**
 * 初始化日志参数
 */
func init() {
	Register(AdapterConsole, &consoleLogger{
		LogLevel: LevelTrace,
		Colorful: runtime.GOOS != "windows",
	})
}
