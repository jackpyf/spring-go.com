package log4g

var Log *LocalLogger

func init() {
	Log = NewLog4g(LevelTrace, "APP...", "spring-go.com")
}

func NewLog4g(level int, catalog string, pkg string) *LocalLogger {
	logger := NewLogger(level, catalog, pkg)
	return logger
}

func CRITICAL(format string, v ...interface{}) {
	Log.writeMsg(LevelCritical, format, v...)
}

func ERROR(format string, v ...interface{}) {
	Log.writeMsg(LevelError, format, v...)
}

func WARN(format string, v ...interface{}) {
	Log.writeMsg(LevelWarning, format, v...)
}

func INFO(format string, v ...interface{}) {
	Log.writeMsg(LevelInfo, format, v...)
}

func DEBUG(format string, v ...interface{}) {
	Log.writeMsg(LevelDebug, format, v...)
}

func TRACE(format string, v ...interface{}) {
	Log.writeMsg(LevelTrace, format, v...)
}
