package log4g

import (
	"fmt"
	"os"
)

type LogConfig struct {
	Format  string        `json:"format"`
	Console consoleLogger `json:"console"`
	File    fileLogger    `json:"file"`
}

func fileToString(jsonConfig string) (string, error) {
	file, err := os.Open(jsonConfig)
	defer file.Close()
	var buffer []byte
	if err != nil {
		fmt.Println(err)
	} else {
		fileinfo, err := file.Stat()
		if err != nil {
			fmt.Println(err)
		}
		filesize := fileinfo.Size()
		buffer = make([]byte, filesize)
		_, err = file.Read(buffer)
		if err != nil {
			fmt.Println(err)
		}
	}
	return string(buffer), err
}
