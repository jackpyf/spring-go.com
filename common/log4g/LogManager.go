package log4g

import (
	"encoding/json"
	"fmt"
	"os"
	"runtime"
	"spring-go.com/common/utils"
	"strings"
	"sync"
	"time"
)

// 日志等级，从0-5，日优先级由高到低
const (
	LevelCritical = iota // 系统级危险，比如磁盘出错，内存异常，网络不可用，数据库访问异常，配置文件出错，权限出错，访问异常等
	LevelError           // 用户级错误
	LevelWarning         // 用户级警告
	LevelInfo            // 用户级信息
	LevelDebug           // 用户级调试
	LevelTrace           // 用户级基本输出
)

// 日志等级和描述映射关系
var LevelMap = map[string]int{
	"CRITL": LevelCritical,
	"ERORR": LevelError,
	"WARN ": LevelWarning,
	"INFO ": LevelInfo,
	"DEBUG": LevelDebug,
	"TRACE": LevelTrace,
}

// 日志记录等级字段
var levelPrefix = []string{
	"CRITL",
	"ERORR",
	"WARN ",
	"INFO ",
	"DEBUG",
	"TRACE",
}

const (
	logTimeDefaultFormat = "2006-01-02 03:04:05.000" // 日志输出默认格式
	AdapterConsole       = "console"                 // 控制台输出配置项
	AdapterFile          = "file"                    // 文件输出配置项
)

// logger接口
type Logger interface {
	InitConfig(config interface{}) error
	LogWrite(when time.Time, msg interface{}, level int) error
	Destroy()
}

// 注册实现的适配器， 当前支持控制台，文件和网络输出
var adapters = make(map[string]Logger)

type LocalLogger struct {
	Lock       sync.Mutex
	Adapters   []*nameLogger
	TimeFormat string
	Catalog    string
	Package    string
}

type nameLogger struct {
	Logger
	name   string
	config interface{}
}

// 日志输出适配器注册，log需要实现Init，LogWrite，Destroy方法
func Register(name string, log Logger) {
	if log == nil {
		panic("logs: Register provide is nil")
	}
	if _, ok := adapters[name]; ok {
		panic("logs: Register called twice for provider " + name)
	}
	adapters[name] = log
}

func init() {
}

func NewLogger(logType int, catalog string, pkg string) *LocalLogger {
	l := new(LocalLogger)
	l.TimeFormat = logTimeDefaultFormat
	l.Catalog = catalog
	if len(pkg) > 0 {
		l.Package = pkg
	} else {
		l.Package = "spring-go.com"
	}
	if logType < 0 {
		logType = LevelInfo
	}
	if logType > 6 {
		logType = LevelInfo
	}
	l.setLogger(AdapterConsole, &consoleLogger{
		Level:    levelPrefix[logType],
		Colorful: runtime.GOOS != "windows",
	})
	return l
}
func InitLogger(configName string) *LocalLogger {
	l := new(LocalLogger)
	l.Package = "spring-go.com"
	if tmp, err := fileToString(configName); err != nil {
		l.TimeFormat = logTimeDefaultFormat
		return l
	} else {
		var jsonConfig LogConfig
		err := json.Unmarshal([]byte(tmp), &jsonConfig)
		if err != nil {
			fmt.Printf("err was %v", err)
		}
		if len(jsonConfig.Console.Level) > 0 {
			l.setLogger(AdapterConsole, &jsonConfig.Console)
		}
		if len(jsonConfig.File.Level) > 0 {
			l.setLogger(AdapterFile, &jsonConfig.File)
		}
		if len(jsonConfig.Format) != 0 {
			l.TimeFormat = jsonConfig.Format
		} else {
			l.TimeFormat = logTimeDefaultFormat
		}
		return l
	}
}

func (this *LocalLogger) setConsole() error {
	adapterName := AdapterConsole
	this.Lock.Lock()
	defer this.Lock.Unlock()
	if len(this.Adapters) == 0 {
		this.Adapters = []*nameLogger{}
	}
	logger, ok := adapters[adapterName]
	if !ok {
		return fmt.Errorf("unknown adaptername %s (forgotten Register?)", adapterName)
	}
	this.Adapters = append(this.Adapters, &nameLogger{name: adapterName, Logger: logger, config: nil})
	return nil
}
func (this *LocalLogger) setLogger(adapterName string, config interface{}) error {
	this.Lock.Lock()
	defer this.Lock.Unlock()
	if len(this.Adapters) == 0 {
		this.Adapters = []*nameLogger{}
	}
	logger, ok := adapters[adapterName]
	if !ok {
		return fmt.Errorf("unknown adaptername %s (forgotten Register?)", adapterName)
	}
	err := logger.InitConfig(config) //获取到相应的logger以后，console/file。然后去做初始化，从配置文件里面获取初始化信息
	if err != nil {
		fmt.Fprintf(os.Stderr, "log4g Init <%s> err:%v, %s output ignore!\n",
			adapterName, err, adapterName)
		return err
	}
	this.Adapters = append(this.Adapters, &nameLogger{name: adapterName, Logger: logger, config: config})
	return nil
}

func (this *LocalLogger) CRITICAL(format string, v ...interface{}) {
	this.writeMsg(LevelCritical, format, v...)
	//this.Emer("###Exec Panic:"+format, args...)
	//panic(fmt.Sprintf(format, v...))
}

func (this *LocalLogger) ERROR(format string, v ...interface{}) {
	this.writeMsg(LevelError, format, v...)
}

func (this *LocalLogger) WARN(format string, v ...interface{}) {
	this.writeMsg(LevelWarning, format, v...)
}

func (this *LocalLogger) INFO(format string, v ...interface{}) {
	this.writeMsg(LevelInfo, format, v...)
}

func (this *LocalLogger) DEBUG(format string, v ...interface{}) {
	this.writeMsg(LevelDebug, format, v...)
}

func (this *LocalLogger) TRACE(format string, v ...interface{}) {
	this.writeMsg(LevelTrace, format, v...)
}

func (this *LocalLogger) writeMsg(logLevel int, msg string, v ...interface{}) error {
	msgSt := new(logInfo)
	src := ""
	if len(v) > 0 {
		paramMsg := ""
		for _, val := range v {
			paramMsg = paramMsg + fmt.Sprintf("%v", val)
		}
		msg = fmt.Sprintf("%s%v", msg, paramMsg)
	}
	when := time.Now()
	_, file, lineno, ok := runtime.Caller(2)

	file = utils.SubString(file, strings.Index(file, this.Package)+14, len(file))
	if ok {
		src = strings.Replace(
			fmt.Sprintf("%s:%d", file, lineno), "%2e", ".", -1)
	}
	msgSt.Level = levelPrefix[logLevel]
	msgSt.Path = src
	msgSt.Content = msg
	msgSt.Time = when.Format(this.TimeFormat)
	this.writeToLoggers(when, msgSt, logLevel)
	return nil
}

type logInfo struct {
	Time    string
	Level   string
	Path    string
	Name    string
	Content string
}

func (this *LocalLogger) writeToLoggers(when time.Time, msg *logInfo, level int) {
	for _, l := range this.Adapters {
		msgStr := when.Format(this.TimeFormat) + " [" + this.Catalog + "] [" + msg.Level + "] " + "[" + msg.Path + "] " + msg.Content
		err := l.LogWrite(when, msgStr, level)
		if err != nil {
			fmt.Fprintf(os.Stderr, "unable to WriteMsg to adapter:%v,error:%v\n", l.name, err)
		}
	}
}

func (this *LocalLogger) Reset() {
	for _, l := range this.Adapters {
		l.Destroy()
	}
	this.Adapters = nil
}
