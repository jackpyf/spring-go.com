package kafka

import (
	"fmt"
	"github.com/Shopify/sarama"
	"spring-go.com/common/middleware/kafka/single"
	"strings"
	"time"
)

type KafkaHelper struct {
	Produer  single.Producer
	Consumer single.Consumer
	Batch    single.KafkaRunner
}

func NewKafkaHelper(kafkaConfig single.KafKaConfig) *KafkaHelper {
	producer := single.Producer{KafkaConfig: &kafkaConfig}
	if sync, err := producer.GetSyncProducer(); err != nil {
		panic(err)
	} else {
		producer.SyncProducer = &sync
	}
	if async, err := producer.GetAsyncProducer(); err != nil {
		panic(err)
	} else {
		producer.AsyncProducer = &async
	}
	consumer := single.Consumer{KafkaConfig: &kafkaConfig}
	//配置
	config := sarama.NewConfig()
	//接收失败通知
	config.Consumer.Return.Errors = true
	//设置使用的kafka版本,如果低于V0_10_0_0版本,消息中的timestrap没有作用.需要消费和生产同时配置
	config.Version = sarama.V2_2_0_0
	//新建一个消费者
	if c, err := sarama.NewConsumer(strings.Split(consumer.KafkaConfig.KafkaServer, ","), config); err != nil {
		panic(err)
	} else {
		consumer.Consumer = &c
	}
	return &KafkaHelper{Produer: producer, Consumer: consumer}
}

func (this *KafkaHelper) Close() {
	this.Consumer.Close()
	this.Produer.Close()
}
func (this *KafkaHelper) Run() {
	this.Consumer.Callback = this.Batch.Runner
	this.Consumer.Execute()
}

func (this *KafkaHelper) SendAsyncMessage(message string) {
	if producer := (*this.Produer.AsyncProducer); producer == nil {
		panic("There is no async producer")
		return
	} else {
		//必须有这个匿名函数内容
		go func(producer sarama.AsyncProducer) {
			errors := producer.Errors()
			success := producer.Successes()
			for {
				select {
				case err := <-errors:
					if err != nil {
						fmt.Print(err)
					}
				case <-success:
				}
			}
		}(producer)
		msg := &sarama.ProducerMessage{
			Topic:     this.Produer.KafkaConfig.Topic,
			Value:     sarama.ByteEncoder(message),
			Timestamp: time.Now(),
		}
		producer.Input() <- msg
	}
}

func (this *KafkaHelper) SendSyncMessage(message string) {
	msg := &sarama.ProducerMessage{
		Topic: this.Produer.KafkaConfig.Topic,
		Value: sarama.ByteEncoder(message),
	}

	if this.Produer.SyncProducer == nil {
		panic("There is no sync producer")
		return
	}
	if _, _, err := (*this.Produer.SyncProducer).SendMessage(msg); err != nil {
		fmt.Print(err)
		return
	}
}
