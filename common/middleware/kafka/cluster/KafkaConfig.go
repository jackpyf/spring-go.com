package cluster

import (
	"fmt"
	"github.com/Shopify/sarama"
	"strings"
)

type KafKaConfig struct {
	Topics      string //,
	KafkaServer string //,
	Partition   int32
	RequireAcks sarama.RequiredAcks
	GroupID     string
}

func Metadata(kafkaServer string) {
	fmt.Printf("metadata testn")

	config := sarama.NewConfig()
	config.Version = sarama.V0_11_0_2

	client, err := sarama.NewClient(strings.Split(kafkaServer, ","), config)
	if err != nil {
		fmt.Printf("metadata_test try create client err :%sn", err.Error())
		return
	}

	defer client.Close()

	// get topic set
	topics, err := client.Topics()
	if err != nil {
		fmt.Printf("try get topics err %sn", err.Error())
		return
	}

	fmt.Printf("topics(%d):n", len(topics))

	for _, topic := range topics {
		fmt.Println(topic)
	}

	// get broker set
	brokers := client.Brokers()
	fmt.Printf("broker set(%d):n", len(brokers))
	for _, broker := range brokers {
		fmt.Printf("%sn", broker.Addr())
	}
}
