package cluster_consumer

import (
	"fmt"
	"github.com/Shopify/sarama"
	saramaCluster "github.com/bsm/sarama-cluster"
	"log"
	"os"
	"os/signal"
	"spring-go.com/common/middleware/kafka/cluster"
	"strings"
	"syscall"
)

func ClusterConsumer(kafkaConfig *cluster.KafKaConfig, callback func(message *sarama.ConsumerMessage)) {
	topics := kafkaConfig.Topics
	kafkaServer := kafkaConfig.KafkaServer
	groupID := kafkaConfig.GroupID
	//配置
	config := saramaCluster.NewConfig()
	//接收失败通知
	config.Consumer.Return.Errors = true
	config.Group.Return.Notifications = true
	//设置使用的kafka版本,如果低于V0_10_0_0版本,消息中的timestrap没有作用.需要消费和生产同时配置
	config.Version = sarama.V2_2_0_0
	//新建一个消费者
	consumer, err := saramaCluster.NewConsumer(strings.Split(kafkaServer, ","), groupID, strings.Split(topics, ","), config)
	if err != nil {
		panic("error get consumer")
	}
	defer consumer.Close()

	// Create signal channel
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)

	// Consume all channels, wait for signal to exit
	for {
		select {
		case msg, more := <-consumer.Messages():
			if more {
				callback(msg)
				//fmt.Fprintf(os.Stdout, "%s/%d/%d\t%s\n", msg.Topic, msg.Partition, msg.Offset, msg.Value)
				consumer.MarkOffset(msg, "")
			}
		case ntf, more := <-consumer.Notifications():
			if more {
				log.Printf("Rebalanced: %+v\n", ntf)
			}
		case err, more := <-consumer.Errors():
			if more {
				log.Printf("Error: %s\n", err.Error())
			}
		case <-sigchan:
			return
		}
	}
}

func ClusterPartitionConsumer(kafkaConfig *cluster.KafKaConfig, callback func(message *sarama.ConsumerMessage)) {
	topics := kafkaConfig.Topics
	kafkaServer := kafkaConfig.KafkaServer
	groupID := kafkaConfig.GroupID
	//配置
	config := saramaCluster.NewConfig()
	//接收失败通知
	config.Consumer.Return.Errors = true
	config.Group.Return.Notifications = true
	//设置使用的kafka版本,如果低于V0_10_0_0版本,消息中的timestrap没有作用.需要消费和生产同时配置
	config.Version = sarama.V2_2_0_0
	//新建一个消费者
	consumer, err := saramaCluster.NewConsumer(strings.Split(kafkaServer, ","), groupID, strings.Split(topics, ","), config)
	if err != nil {
		panic("error get consumer")
	}
	defer consumer.Close()

	// Create signal channel
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)

	// Consume all channels, wait for signal to exit
	for {
		select {
		case part, ok := <-consumer.Partitions():
			if !ok {
				return
			}
			// start a separate goroutine to consume messages
			go func(pc saramaCluster.PartitionConsumer) {
				for msg := range pc.Messages() {
					fmt.Fprintf(os.Stdout, "%s/%d/%d\t%s\t%s\n", msg.Topic, msg.Partition, msg.Offset, msg.Key, msg.Value)
					consumer.MarkOffset(msg, "") // mark message as processed
				}
			}(part)
		case ntf, more := <-consumer.Notifications():
			if more {
				log.Printf("Rebalanced: %+v\n", ntf)
			}
		case err, more := <-consumer.Errors():
			if more {
				log.Printf("Error: %s\n", err.Error())
			}
		case <-sigchan:
			return
		}
	}
}
