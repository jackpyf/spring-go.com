package cluster_producer

import "github.com/Shopify/sarama"

type Producer struct {
	producer        sarama.AsyncProducer
	Id              int
	ProducerGroupId string
}
