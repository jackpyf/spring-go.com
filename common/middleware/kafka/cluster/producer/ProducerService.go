package cluster_producer

import (
	"github.com/Shopify/sarama"
	"github.com/golang/glog"
	"spring-go.com/common/middleware/kafka/cluster"
	"strings"
	"time"
)

func SyncProducer(kafkaConfig *cluster.KafKaConfig, message string) {
	topics := kafkaConfig.Topics
	kafkaServer := kafkaConfig.KafkaServer
	requireAcks := kafkaConfig.RequireAcks

	config := sarama.NewConfig()
	config.Producer.RequiredAcks = requireAcks
	config.Producer.Return.Successes = true
	config.Producer.Timeout = 5 * time.Second
	p, err := sarama.NewSyncProducer(strings.Split(kafkaServer, ","), config)
	defer p.Close()
	if err != nil {
		glog.Errorln(err)
		return
	}

	msg := &sarama.ProducerMessage{
		Topic: topics,
		Value: sarama.ByteEncoder(message),
	}
	if _, _, err := p.SendMessage(msg); err != nil {
		glog.Errorln(err)
		return
	}
}

func AsyncProducer(kafkaConfig *cluster.KafKaConfig, message string) {
	topic := kafkaConfig.Topics
	kafkaServer := kafkaConfig.KafkaServer

	config := sarama.NewConfig()
	config.Producer.Return.Successes = true //必须有这个选项
	config.Producer.Timeout = 5 * time.Second
	p, err := sarama.NewAsyncProducer(strings.Split(kafkaServer, ","), config)
	defer p.Close()
	if err != nil {
		return
	}
	//必须有这个匿名函数内容

	go func(p sarama.AsyncProducer) {
		errors := p.Errors()
		success := p.Successes()
		for {
			select {
			case err := <-errors:
				if err != nil {
					glog.Errorln(err)
				}
			case <-success:
			}
		}
	}(p)
	msg := &sarama.ProducerMessage{
		Topic:     topic,
		Value:     sarama.ByteEncoder(message),
		Timestamp: time.Now(),
	}
	p.Input() <- msg
}
