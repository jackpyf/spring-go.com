package single

import (
	"github.com/Shopify/sarama"
)

type Consumer struct {
	KafkaConfig *KafKaConfig
	Consumer    *sarama.Consumer
	Callback    func(message *KafkaMessage)
}
