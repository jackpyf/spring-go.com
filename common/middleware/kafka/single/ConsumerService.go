package single

import (
	"fmt"
	"github.com/Shopify/sarama"
	"strings"
)

func NewKafkaConsumer(kafkaConfig KafKaConfig) (Consumer, error) {
	consumer := Consumer{KafkaConfig: &kafkaConfig}
	//配置
	config := sarama.NewConfig()
	//接收失败通知
	config.Consumer.Return.Errors = true
	//设置使用的kafka版本,如果低于V0_10_0_0版本,消息中的timestrap没有作用.需要消费和生产同时配置
	config.Version = sarama.V2_2_0_0
	//新建一个消费者
	if c, err := sarama.NewConsumer(strings.Split(consumer.KafkaConfig.KafkaServer, ","), config); err != nil {
		panic("error get consumer")
	} else {
		consumer.Consumer = &c
	}
	return consumer, nil
}

func (this *Consumer) Execute() {
	//根据消费者获取指定的主题分区的消费者,Offset这里指定为获取最新的消息.
	partitionConsumer, err := (*this.Consumer).ConsumePartition(this.KafkaConfig.Topic, this.KafkaConfig.Partition, sarama.OffsetNewest)

	if err != nil {
		fmt.Println("error get partition consumer", err)
	}
	//循环等待接受消息.
	for {
		select {
		//接收消息通道和错误通道的内容.
		case msg := <-partitionConsumer.Messages():
			this.Callback(&KafkaMessage{msg})
		case err := <-partitionConsumer.Errors():
			fmt.Println(err.Err)
		}
	}
}

func (this *Consumer) Close() {
	(*this.Consumer).Close()
}
