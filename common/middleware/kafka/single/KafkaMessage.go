package single

import "github.com/Shopify/sarama"

type KafkaMessage struct {
	Message *sarama.ConsumerMessage
}
