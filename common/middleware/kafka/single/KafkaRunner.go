package single

type KafkaRunner interface {
	Runner(msg *KafkaMessage)
}
