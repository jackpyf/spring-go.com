package single

import (
	"github.com/Shopify/sarama"
)

type Producer struct {
	KafkaConfig   *KafKaConfig
	SyncProducer  *sarama.SyncProducer
	AsyncProducer *sarama.AsyncProducer
}
