package single

import (
	"fmt"
	"github.com/Shopify/sarama"
	"strings"
	"time"
)

func NewKafkaProducer(kafkaConfig KafKaConfig) (Producer, error) {
	producer := Producer{KafkaConfig: &kafkaConfig}
	if sync, err := producer.GetSyncProducer(); err != nil {
		panic(err)
	} else {
		producer.SyncProducer = &sync
	}
	if async, err := producer.GetAsyncProducer(); err != nil {
		panic(err)
	} else {
		producer.AsyncProducer = &async
	}
	return producer, nil
}
func (this *Producer) GetSyncProducer() (sarama.SyncProducer, error) {
	kafkaServer := this.KafkaConfig.KafkaServer
	requireAcks := this.KafkaConfig.RequireAcks
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = requireAcks
	config.Producer.Return.Successes = true
	config.Producer.Timeout = 5 * time.Second
	p, err := sarama.NewSyncProducer(strings.Split(kafkaServer, ","), config)
	//defer p.Close()
	if err != nil {
		return nil, err
	}
	return p, nil
}

func (this *Producer) GetAsyncProducer() (sarama.AsyncProducer, error) {
	kafkaServer := this.KafkaConfig.KafkaServer
	requireAcks := this.KafkaConfig.RequireAcks
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = requireAcks
	config.Producer.Return.Successes = true
	config.Producer.Timeout = 5 * time.Second
	p, err := sarama.NewAsyncProducer(strings.Split(kafkaServer, ","), config)
	//defer p.Close()
	if err != nil {
		return nil, err
	}
	return p, nil
}
func (this *Producer) SendSyncMessage(message string) {
	fmt.Println("SendSyncMessage...")
	fmt.Println(this)
	msg := &sarama.ProducerMessage{
		Topic: this.KafkaConfig.Topic,
		Value: sarama.ByteEncoder(message),
	}

	if this.SyncProducer == nil {
		panic("There is no sync producer")
		return
	}
	if _, _, err := (*this.SyncProducer).SendMessage(msg); err != nil {
		fmt.Print(err)
		return
	}
}

func (this *Producer) SendAsyncMessage(message string) {
	if producer := (*this.AsyncProducer); producer == nil {
		panic("There is no async producer")
		return
	} else {
		//必须有这个匿名函数内容
		go func(producer sarama.AsyncProducer) {
			errors := producer.Errors()
			success := producer.Successes()
			for {
				select {
				case err := <-errors:
					if err != nil {
						fmt.Print(err)
					}
				case <-success:
				}
			}
		}(producer)
		msg := &sarama.ProducerMessage{
			Topic:     this.KafkaConfig.Topic,
			Value:     sarama.ByteEncoder(message),
			Timestamp: time.Now(),
		}
		producer.Input() <- msg
	}
}

func (this *Producer) Close() {
	(*this.SyncProducer).Close()
	(*this.AsyncProducer).Close()
}
