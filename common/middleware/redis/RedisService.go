package redis

import (
	"errors"
	"fmt"
	"github.com/garyburd/redigo/redis"
)

type RedisService struct {
	Pool     *redis.Pool
	Password string
}

func (this *RedisService) getRedisConn() redis.Conn {
	c := this.Pool.Get()
	if len(this.Password) > 0 {
		c.Do("AUTH", this.Password)
	}
	return c
}
func (this *RedisService) GetString(key string) (string, error) {
	c := this.getRedisConn()
	defer c.Close()
	val, err := redis.String(c.Do("GET", key))
	if err != nil {
		return "", nil
	}
	return val, nil
}

func (this *RedisService) GetBool(key string) (bool, error) {
	c := this.getRedisConn()
	defer c.Close()
	val, err := redis.Bool(c.Do("GET", key))
	if err != nil {
		return false, nil
	}
	return val, nil
}

func (this *RedisService) GetInt(key string) (int, error) {
	c := this.getRedisConn()
	defer c.Close()
	val, err := redis.Int(c.Do("GET", key))
	if err != nil {
		return 0, nil
	}
	return val, nil
}

func (this *RedisService) IsExists(key string) (bool, error) {
	c := this.getRedisConn()
	defer c.Close()
	if val, err := redis.Bool(c.Do("EXISTS", key)); err != nil {
		fmt.Println("error:", err)
		return false, err
	} else {
		return val, nil
	}
}

func (this *RedisService) DelKey(key string) (bool, error) {
	c := this.getRedisConn()
	defer c.Close()
	if _, err := c.Do("DEL", key); err != nil {
		return false, err
	} else {
		return true, nil
	}
}

func (this *RedisService) Set(key string, value interface{}) (interface{}, error) {
	c := this.getRedisConn()
	defer c.Close()
	val, err := c.Do("SET", key, value)
	if err != nil {
		return val, err
	}
	return val, nil
}

func (this *RedisService) SetNX(key string, value string) (bool, error) {
	c := this.getRedisConn()
	defer c.Close()
	if val, err := c.Do("SETNX", key, value); err != nil {
		return false, err
	} else {
		if val == int64(1) {
			return true, nil
		}
		return false, errors.New("Why?")
	}
}

func (this *RedisService) Expire(key string, value int) (bool, error) {
	c := this.getRedisConn()
	defer c.Close()
	if val, err := c.Do("EXPIRE", key, value); err != nil {
		return false, err
	} else {
		if val == int64(1) {
			return true, nil
		}
		return false, errors.New("Why?")
	}
}

func (this *RedisService) LPush(key string, value interface{}) (bool, error) {
	c := this.getRedisConn()
	defer c.Close()
	if _, err := c.Do("lpush", key, value); err != nil {
		return false, err
	}
	return true, nil
}

func (this *RedisService) LRang(key string, start int, end int) ([]interface{}, error) {
	c := this.getRedisConn()
	defer c.Close()
	if val, err := redis.Values(c.Do("lrange", key, start, end)); err != nil {
		return nil, err
	} else {
		return val, nil
	}
}
