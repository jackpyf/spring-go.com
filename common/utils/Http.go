package utils

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net/http"
)

func HttpGet(address string) string {
	resp, err := http.Get(address)
	if err != nil {
		// handle error
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
	}
	return string(body)

}

func HttpPost(address string, params *bufio.Reader) string {
	resp, err := http.Post(address,
		"application/x-www-form-urlencoded", params)
	//strings.NewReader("name=cjb")
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
	}
	return string(body)
}
func httpPostForm(address string, maps map[string][]string) string {
	resp, err := http.PostForm(address, maps)
	//url.Values{"key": {"Value"}, "id": {"123"}}
	if err != nil {
		// handle error
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
	}
	return string(body)
}
