package utils

import (
	"reflect"
	"runtime"
	"strconv"
	"strings"
)

func SubString(str string, begin, length int) (substr string) {
	// 将字符串的转换成[]rune
	rs := []rune(str)
	lth := len(rs)

	// 简单的越界判断
	if begin < 0 {
		begin = 0
	}
	if begin >= lth {
		begin = lth
	}
	end := begin + length
	if end > lth {
		end = lth
	}

	// 返回子串
	return string(rs[begin:end])
}

func GetControllerName(str string) string {
	kv := strings.Split(str, ".")
	if len(kv) == 0 {
		return ""
	} else {
		key := kv[len(kv)-1]
		key = strings.Replace(key, "Controller", "", -1)
		key = ToLowerStart(key)
		return key
	}
}

func IsLowerStart(input string) bool {
	temp := SubString(input, 0, 1)
	if temp == strings.ToLower(temp) {
		return true
	}
	return false
}
func IsUpperStart(input string) bool {
	return !IsLowerStart(input)
}
func ConvertToGetMethod(field string) (getMethod string) {
	getMethod = "Get" + ToUpperStart(field)
	return
}
func ConvertToSetMethod(field string) (setMethod string) {
	setMethod = "Set" + ToUpperStart(field)
	return
}
func ToLowerStart(input string) (output string) {
	temp := SubString(input, 0, 1)
	temp = strings.ToLower(temp)
	output = temp + input[1:]
	return
}
func ToUpperStart(input string) (output string) {
	temp := SubString(input, 0, 1)
	temp = strings.ToUpper(temp)
	output = temp + input[1:]
	return
}

func Str2bytes(s string) []byte {
	p := make([]byte, len(s))
	for i := 0; i < len(s); i++ {
		c := s[i]
		p[i] = c
	}
	return p
}

// snake string, XxYy to xx_yy , XxYY to xx_yy
func SnakeString(s string) string {
	data := make([]byte, 0, len(s)*2)
	j := false
	num := len(s)
	for i := 0; i < num; i++ {
		d := s[i]
		if i > 0 && d >= 'A' && d <= 'Z' && j {
			data = append(data, '_')
		}
		if d != '_' {
			j = true
		}
		data = append(data, d)
	}
	return strings.ToLower(string(data[:]))
}

// camel string, xx_yy to XxYy
func CamelString(s string) string {
	data := make([]byte, 0, len(s))
	j := false
	k := false
	num := len(s) - 1
	for i := 0; i <= num; i++ {
		d := s[i]
		if k == false && d >= 'A' && d <= 'Z' {
			k = true
		}
		if d >= 'a' && d <= 'z' && (j || k == false) {
			d = d - 32
			j = false
			k = true
		}
		if k && d == '_' && num > i && s[i+1] >= 'a' && s[i+1] <= 'z' {
			j = true
			continue
		}
		data = append(data, d)
	}
	return string(data[:])
}

// Replace the nth occurrence of old in s by new.
func replaceNth(s, old, new string, n int) string {
	i := 0
	for m := 1; m <= n; m++ {
		x := strings.Index(s[i:], old)
		if x < 0 {
			break
		}
		i += x
		if m == n {
			return s[:i] + new + s[i+len(old):]
		}
		i += len(old)
	}
	return s
}

func GetCAndMFromURI(input string) (string, string) {
	for strings.Index(input, "//") >= 0 {
		input = strings.Replace(input, "//", "/", -1)
	}
	tmp := strings.Split(input, "?")
	tmp3 := strings.Split(tmp[0], "/")
	if len(tmp3) > 2 {
		return tmp3[1], tmp3[2]
	} else {
		return "", ""
	}
}
func GetBeanKey(a string, b string) string {
	return a + "_" + b
}

func strToUint(strNumber string, value interface{}) (err error) {
	var number interface{}
	number, err = strconv.ParseUint(strNumber, 10, 64)
	switch v := number.(type) {
	case uint64:
		switch d := value.(type) {
		case *uint64:
			*d = v
		case *uint:
			*d = uint(v)
		case *uint16:
			*d = uint16(v)
		case *uint32:
			*d = uint32(v)
		case *uint8:
			*d = uint8(v)
		}
	}
	return
}
func IsSliceType(t reflect.Type) bool {
	switch t.String() {
	case "[...]int":
		return true
	case "[...]bool":
		return true
	case "[...]string":
		return true
	case "[...]rune":
		return true
	case "[...]byte":
		return true
	case "[...]uint":
		return true
	case "[...]int8":
		return true
	case "[...]uint8":
		return true
	case "[...]int16":
		return true
	case "[...]uint16":
		return true
	case "[...]int32":
		return true
	case "[...]uint32":
		return true
	case "[...]int64":
		return true
	case "[...]uint64":
		return true
	case "[...]float64":
		return true
	case "[...]float32":
		return true
	default:
		_ = t
	}
	return false
}
func IsAvailableArrType(t reflect.Type) bool {
	switch t.String() {
	case "[]int":
		return true
	case "[]bool":
		return true
	case "[]string":
		return true
	case "[]rune":
		return true
	case "[]byte":
		return true
	case "[]uint":
		return true
	case "[]int8":
		return true
	case "[]uint8":
		return true
	case "[]int16":
		return true
	case "[]uint16":
		return true
	case "[]int32":
		return true
	case "[]uint32":
		return true
	case "[]int64":
		return true
	case "[]uint64":
		return true
	case "[]float64":
		return true
	case "[]float32":
		return true
	default:
		_ = t
	}
	return false
}

func IsAvailableType(t reflect.Type) bool {
	switch t.String() {
	case "int":
		return true
	case "bool":
		return true
	case "string":
		return true
	case "rune":
		return true
	case "byte":
		return true
	case "uint":
		return true
	case "int8":
		return true
	case "uint8":
		return true
	case "int16":
		return true
	case "uint16":
		return true
	case "int32":
		return true
	case "uint32":
		return true
	case "int64":
		return true
	case "uint64":
		return true
	case "float64":
		return true
	case "float32":
		return true
	default:
		_ = t
	}
	return false
}

func IsType(t reflect.Type, v string) bool {
	return t.String() == v
}

func ValueConvert(t string, v string) (value interface{}, err error) {
	switch t {
	case "int":
		value, err = strconv.Atoi(v)
		return
	case "bool":
		value, err = strconv.ParseBool(v)
		return
	case "string":
		value = v
		return
	case "rune":
		value = []rune(v)
		return
	case "byte":
		value = []byte(v)
		return
	case "uint":
		var val uint
		strToUint(v, &val)
		value = val
		return
	case "int8":
		value, err = strconv.ParseInt(v, 10, 8)
		return
	case "uint8":
		var val uint8
		strToUint(v, &val)
		value = val
		return
	case "int16":
		value, err = strconv.ParseInt(v, 10, 16)
		return
	case "uint16":
		var val uint16
		strToUint(v, &val)
		value = val
		return
	case "int32":
		value, err = strconv.ParseInt(v, 10, 32)
		return
	case "uint32":
		value, err = strconv.Atoi(v)
		return
	case "int64":
		value, err = strconv.ParseInt(v, 10, 64)
		return
	case "uint64":
		value, err = strconv.Atoi(v)
		return
	case "float64":
		value, err = strconv.ParseFloat(v, 64)
		return
	case "float32":
		value, err = strconv.ParseFloat(v, 32)
		return
	default:
		_ = t
	}
	return
}

func ValuesConvert(t string, v []string) ([]reflect.Value, error) {
	switch t {
	case "[]int":
		var result []int
		for i := 0; i < len(v); i++ {
			val, _ := strconv.Atoi(v[i])
			result = append(result, val)
		}
		return GetValues(result), nil
	case "[]bool":
		var result []bool
		for i := 0; i < len(v); i++ {
			val, _ := strconv.ParseBool(v[i])
			result = append(result, val)
		}
		return GetValues(result), nil
	case "[]string":
		var result []string
		for i := 0; i < len(v); i++ {
			val := v[i]
			result = append(result, val)
		}
		return GetValues(result), nil
	case "[]uint":
		var result []uint
		for i := 0; i < len(v); i++ {
			var tmp uint
			strToUint(v[i], &tmp)
			val := tmp
			result = append(result, val)
		}
		return GetValues(result), nil
	case "[]int8":
		var result []int8
		for i := 0; i < len(v); i++ {
			val, _ := strconv.ParseInt(v[i], 10, 8)
			result = append(result, int8(val))
		}
		return GetValues(result), nil
	case "[]uint8":
		var result []uint8
		for i := 0; i < len(v); i++ {
			var tmp uint8
			strToUint(v[0], &tmp)
			val := tmp
			result = append(result, val)
		}
		return GetValues(result), nil
	case "[]int16":
		var result []int16
		for i := 0; i < len(v); i++ {
			val, _ := strconv.ParseInt(v[i], 10, 16)
			result = append(result, int16(val))
		}
		return GetValues(result), nil
	case "[]uint16":
		var result []uint16
		for i := 0; i < len(v); i++ {
			var tmp uint16
			strToUint(v[i], &tmp)
			val := tmp
			result = append(result, val)
		}
		return GetValues(result), nil
	case "[]int32":
		var result []int32
		for i := 0; i < len(v); i++ {
			val, _ := strconv.ParseInt(v[i], 10, 32)
			result = append(result, int32(val))
		}
		return GetValues(result), nil
	case "[]uint32":
		var result []uint32
		for i := 0; i < len(v); i++ {
			val, _ := strconv.Atoi(v[i])
			result = append(result, uint32(val))
		}
		return GetValues(result), nil
	case "[]int64":
		var result []int64
		for i := 0; i < len(v); i++ {
			val, _ := strconv.ParseInt(v[i], 10, 64)
			result = append(result, val)
		}
		return GetValues(result), nil
	case "[]uint64":
		var result []uint64
		for i := 0; i < len(v); i++ {
			val, _ := strconv.Atoi(v[i])
			result = append(result, uint64(val))
		}
		return GetValues(result), nil
	case "[]float64":
		var result []float64
		for i := 0; i < len(v); i++ {
			val, _ := strconv.ParseFloat(v[i], 64)
			result = append(result, val)
		}
		return GetValues(result), nil
	case "[]float32":
		var result []float32
		for i := 0; i < len(v); i++ {
			val, _ := strconv.ParseFloat(v[i], 32)
			result = append(result, float32(val))
		}
		return GetValues(result), nil
	default:
		_ = t
	}
	return nil, nil
}
func IsController(name string) bool {
	if strings.Index(name, "Controller") > 0 {
		return true
	} else {
		return false
	}
}

/*获取当前文件执行的路径*/
func GetCurrentDirectory() string {
	_, dir, _, _ := runtime.Caller(0)
	return strings.Replace(dir, "\\", "/", -1)
}

func ConvertToString(v reflect.Value) (value string) {
	switch v.Type().String() {
	case "int":
		value = strconv.FormatInt(v.Int(), 10)
		return
	case "bool":
		value = strconv.FormatBool(v.Bool())
		return
	case "string":
		value = v.String()
		return
	case "uint":
		value = strconv.FormatInt(v.Int(), 10)
		return
	case "int8":
		value = strconv.FormatInt(v.Int(), 8)
		return
	case "uint8":
		value = strconv.FormatUint(v.Uint(), 8)
		return
	case "int16":
		value = strconv.FormatInt(v.Int(), 16)
		return
	case "uint16":
		value = strconv.FormatUint(v.Uint(), 16)
		return
	case "int32":
		value = strconv.FormatInt(v.Int(), 32)
		return
	case "uint32":
		value = strconv.FormatUint(v.Uint(), 32)
		return
	case "int64":
		value = strconv.FormatInt(v.Int(), 64)
		return
	case "uint64":
		value = strconv.FormatUint(v.Uint(), 64)
		return
	case "float64":
		value = strconv.FormatFloat(v.Float(), 'g', 5, 64)
		return
	case "float32":
		value = strconv.FormatFloat(v.Float(), 'g', 5, 32)
		return
	default:
		_ = v
	}
	return
}
