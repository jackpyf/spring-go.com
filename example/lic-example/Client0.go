package main

import (
	"io"
	"net"
	"os"
	"spring-go.com/common/log4g"
	"spring-go.com/lic/message"
	"spring-go.com/lic/pack"
	"sync"
	"time"
)

var rw0Mutex sync.RWMutex

func test0Ping(exitChan chan int, conn net.Conn) {
	rw0Mutex.Lock()
	defer rw0Mutex.Unlock()
	sendMsg := message.NewMessage(0, []byte("Ping"))
	p := pack.NewPack()
	msg, _ := p.Pack(sendMsg)
	_, err := conn.Write(msg)
	if err != nil {
		log4g.INFO("Write Error", err)
		return
	}
	msgHeadBinary := make([]byte, p.PackLen())
	_, err = io.ReadFull(conn, msgHeadBinary)
	if err != nil {
		log4g.ERROR("Message Head Error")
		return
	}
	up := pack.NewUnPack()

	msgHead, err := up.UnPack(msgHeadBinary)
	if err != nil {
		log4g.ERROR("UnPack Error", err)
		return
	}

	if msgHead.GetMsgLen() > 0 {

		msg := msgHead.(*message.Message)
		msg.MsgData = make([]byte, msg.GetMsgLen())

		_, err := io.ReadFull(conn, msg.MsgData)
		if err != nil {
			log4g.ERROR("Server UnPack err:", err)
			return
		}

		log4g.INFO("==> RECV MsgId=", msg.GetServiceId(), ", MsgLen=", msg.MsgLen, ", MsgData=", string(msg.MsgData))
	}

	exitChan <- 1
}

func test0Message(exitChan chan int, conn net.Conn) {
	rw0Mutex.Lock()
	defer rw0Mutex.Unlock()

	sendMsg := message.NewMessage(1, []byte("Test Message"))
	p := pack.NewPack()
	msg, _ := p.Pack(sendMsg)
	_, err := conn.Write(msg)
	if err != nil {
		log4g.ERROR("Write Error", err)
		return
	}
	msgHeadBinary := make([]byte, p.PackLen())
	_, err = io.ReadFull(conn, msgHeadBinary)
	if err != nil {
		log4g.ERROR("Message Head Error")
		return
	}
	up := pack.NewUnPack()

	msgHead, err := up.UnPack(msgHeadBinary)
	if err != nil {
		log4g.ERROR("UnPack Error", err)
		return
	}

	if msgHead.GetMsgLen() > 0 {

		msg := msgHead.(*message.Message)
		msg.MsgData = make([]byte, msg.MsgLen)

		_, err := io.ReadFull(conn, msg.MsgData)
		if err != nil {
			log4g.ERROR("Server UnPack err:", err)
			return
		}

		log4g.INFO("==> RECV MsgId=", msg.GetServiceId(), ", MsgLen=", msg.MsgLen, ", MsgData=", string(msg.MsgData))
	}

	exitChan <- 1
}
func main() {
	Loop := 100
	start := time.Now()
	exitChan := make(chan int)

	conn, err := net.Dial("tcp", "127.0.0.1:1234")
	if err != nil {
		log4g.INFO("Start Error")
		return
	}

	log4g.INFO("Client Test ... start")
	//3秒之后发起测试请求，给服务端开启服务的机会

	for i := 0; i < Loop; i++ {
		go test0Ping(exitChan, conn)
		go test0Message(exitChan, conn)
	}
	log4g.INFO("Client Test ... end")
	sum := 0
	for {
		select {
		case v := <-exitChan:
			sum = sum + v
			log4g.INFO("Sum=", sum)
			if sum >= Loop*2 {
				log4g.INFO("Sum=", sum, " Spend ", time.Since(start))
				os.Exit(0)
			}
		}
	}
}
