package main

import (
	"spring-go.com/common/log4g"
	"spring-go.com/lic/connect"
	"spring-go.com/lic/lic"
	"spring-go.com/lic/service"
)

func OnStart(conn connect.IConnect) {
	log4g.INFO("OnConnectStart....")
	//_ = conn.SendMsg(2, []byte("OnConnectStart...."), true)
}
func OnClose(conn connect.IConnect) {
	log4g.INFO("OnConnectClose....")
}

type PingService struct {
	service.Service
}
type TestService struct {
	service.Service
}

func (this *PingService) Process(request connect.IRequest) {
	_ = request.SendMsg(request.GetServiceId(), []byte("Pong"), true)
}

func (this *TestService) Process(request connect.IRequest) {
	_ = request.SendMsg(request.GetServiceId(), request.GetMessage().GetMsgData(), true)
}

func main() {
	//创建一个server句柄
	server := lic.NewLic("", 1234)

	server.OnStart(OnStart)
	server.OnClose(OnClose)
	//
	server.Service(&PingService{})
	server.Service(&TestService{})

	server.Serve()
}
