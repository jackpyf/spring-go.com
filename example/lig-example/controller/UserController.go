package controller

import (
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"spring-go.com/common/log4g"
	"spring-go.com/common/middleware/kafka"
	"spring-go.com/common/middleware/redis"
	"spring-go.com/example/lig-example/service"
	"spring-go.com/lig/constant"
	"spring-go.com/lig/result"
)

type UserController struct {
	A   int
	a   int
	id  int64
	ids []int64
	B   bool
	b   bool
	C   string
	D   rune
	E   byte
	F   int
	G   uint
	H   int8
	I   uint8
	J   int16
	K   uint16
	L   int32
	M   uint32
	N   int64
	O   uint64
	P   float32
	Q   float64
	//R complex64
	//S complex128
	Logger       *log4g.LocalLogger  `autowired`
	ZyService    *service.ZyService  `autowired`
	RedisService *redis.RedisService `autowired`
	KafkaHelper  *kafka.KafkaHelper  `autowired`
}

func (this *UserController) SetA(a int) {
	this.a = a
}

func (this *UserController) SetIds(ids []int64) {
	this.ids = ids
}

func (this *UserController) SetId(id int64) {
	this.id = id
}

func (this *UserController) GetUser() string {

	type data struct {
		Data string `json:"data"`
		A    int    `json:"a"`
	}
	//this.Logger = log4g.NewLogger("APP...")
	this.Logger.INFO("A=", this.A)
	this.Logger.DEBUG("a=", this.a)
	this.Logger.DEBUG("ids=", this.ids)
	this.Logger.DEBUG("GetUser")
	responseBody := &result.ResponseBody{}
	responseBody.Success(data{"Ok", this.a})
	entity := this.ZyService.GetVersion(this.id)
	responseBody.Success(entity)
	returnData, _ := json.Marshal(responseBody)

	this.Logger.DEBUG(string(returnData))
	redisTest, _ := this.RedisService.GetInt("test")
	this.Logger.DEBUG(string(redisTest))

	this.KafkaHelper.SendSyncMessage("Test...")

	return string(returnData)
}

func MessageProcess(msg *sarama.ConsumerMessage) {
	fmt.Println("msg offset: ", msg.Offset, " partition: ", msg.Partition, " Timestamp: ", msg.Timestamp.Format(constant.TIME_FORMAT), " value: ", string(msg.Value))

}

func (this *UserController) SetUser(input string) {
	fmt.Println("setUser")
}
