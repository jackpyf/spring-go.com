package controller

import (
	"encoding/json"
	"spring-go.com/common/log4g"
	"spring-go.com/example/lig-example/service"
	"spring-go.com/lig/result"
)

type ZyController struct {
	A   int
	a   int
	id  int64
	ids []int64
	B   bool
	b   bool
	C   string
	D   rune
	E   byte
	F   int
	G   uint
	H   int8
	I   uint8
	J   int16
	K   uint16
	L   int32
	M   uint32
	N   int64
	O   uint64
	P   float32
	Q   float64
	//R complex64
	//S complex128
	Logger    *log4g.LocalLogger `autowired`
	ZyService *service.ZyService `autowired`
}

func (this *ZyController) SetA(a int) {
	this.a = a
}

func (this *ZyController) Test() int {
	type data struct {
		A int `json:"a"`
	}
	responseBody := &result.ResponseBody{}
	responseBody.Success(data{this.a})
	returnData, _ := json.Marshal(responseBody)
	this.Logger.DEBUG(string(returnData))
	return this.a
}
