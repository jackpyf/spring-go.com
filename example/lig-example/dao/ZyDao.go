package dao

import (
	"fmt"
	"github.com/go-xorm/xorm"
	"reflect"
)

type ZyService interface {
	Insert(zy Zy) (affected int64, err error)
	Get(id int64) (affected int64, err error)
}

func (this *ZyEntity) SetDao(v interface{}) {
	if engine, ok := (v).(*xorm.Engine); ok == true {
		dao = engine
	} else {
		fmt.Println("type: ", reflect.TypeOf(v))
		fmt.Println("value: ", reflect.ValueOf(v))
		panic("xorm.Engine is not found")
	}
}
