package dao

import (
	"crypto/md5"
	"fmt"
	"github.com/go-xorm/xorm"
	"spring-go.com/common/log4g"
	"time"
)

type ZyEntity struct {
	Zy
	Logger *log4g.LocalLogger `autowired`
}

var (
	dao    *xorm.Engine
	getmd5 = md5.New()
	t      = time.Now()
)

func (this *ZyEntity) Insert(zy Zy) (affected int64, err error) {
	if affected, err = dao.Insert(zy); err != nil {
		fmt.Println(err)
	}
	return
}
func (this *ZyEntity) Get(id int64) Zy {
	var entity Zy
	entity.Id = id
	if has, err := dao.Get(&entity); err == nil {
		this.Logger.DEBUG("has=", has)
	}
	return entity
}
