package dao

import "time"

type Zy struct {
	Id         int64     `json:"id" xorm:"pk autoincr comment('主键ID') BIGINT(20)"`
	Version    string    `json:"version" xorm:"comment('支持最大8个验证码') VARCHAR(8)"`
	Content    string    `json:"content" xorm:"comment('支持最大100个字符的短信') VARCHAR(100)"`
	CreateTime time.Time `json:"create_time" xorm:"not null default '0000-00-00 00:00:00' DATETIME"`
	UpdateTime time.Time `json:"update_time" xorm:"default '0000-00-00 00:00:00' DATETIME"`
}
