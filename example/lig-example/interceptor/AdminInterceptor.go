package interceptor

import (
	"net/http"
	"spring-go.com/common/log4g"
)

type AdminInterceptor struct {
	//W      http.ResponseWriter
	R      *http.Request
	Logger *log4g.LocalLogger `autowired`
}

//false：statusCode=401，true：statusCode=200
func (this *AdminInterceptor) Interceptor() (bool, string) {
	this.Logger.DEBUG("XXXXXXX")
	return true, "Error"
}
