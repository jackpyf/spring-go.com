package main

import (
	"github.com/Shopify/sarama"
	_ "github.com/go-sql-driver/mysql"
	"spring-go.com/common/log4g"
	"spring-go.com/common/middleware/kafka"
	"spring-go.com/common/middleware/kafka/single"
	"spring-go.com/common/middleware/redis"
	"spring-go.com/example/lig-example/main/support"
	"spring-go.com/example/lig-example/service"
	"spring-go.com/lig/lig"
)

func main() {
	engine, _ := support.GetDatasource()
	defer engine.Close()

	app := lig.NewMVC(log4g.LevelTrace)

	app.UseEngine(engine)

	redisService := &redis.RedisService{Pool: support.GetRedisPool(), Password: ""}

	kafKaConfig := single.KafKaConfig{Topic: "test", KafkaServer: "192.168.1.20:9092", Partition: 0, RequireAcks: sarama.WaitForAll}
	kafkaHelper := kafka.NewKafkaHelper(kafKaConfig)

	kafkaService := &service.KafkaService{}
	kafkaHelper.Batch = kafkaService

	app.Install(redisService)
	app.Install((&support.Factory{}).GetBeans())
	app.Install(support.GetProducer())
	app.Install(kafkaHelper)
	app.Install(kafkaService)

	//注入所有bean
	app.BuildResources()

	//后台启动程序，执行类的Exec方法，线程处理，自己自行阻塞，否则只执行一次。
	app.Runback(kafkaHelper) //接收消息

	//app.Interceptors(&interceptor.AdminInterceptor{})

	app.Publish(":8080")
}
