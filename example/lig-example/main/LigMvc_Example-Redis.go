package main

import (
	_ "github.com/go-sql-driver/mysql"
	"spring-go.com/common/log4g"
	"spring-go.com/common/middleware/redis"
	"spring-go.com/example/lig-example/main/support"
	"spring-go.com/lig/lig"
)

func main() {
	engine, _ := support.GetDatasource()
	defer engine.Close()

	app := lig.NewMVC(log4g.LevelTrace)

	app.UseEngine(engine)

	redisService := &redis.RedisService{Pool: support.GetRedisPool(), Password: ""}
	app.Install((&support.Factory{}).GetBeans())
	app.Install(redisService)
	//app.Interceptors(&interceptor.AdminInterceptor{})

	//redisService.Set("test", 1)
	//fmt.Println(redisService.GetInt("test"))

	app.BuildResources()
	app.Publish(":8080")
}
