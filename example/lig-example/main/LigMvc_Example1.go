package main

import (
	_ "github.com/go-sql-driver/mysql"
	"spring-go.com/common/log4g"
	"spring-go.com/example/lig-example/controller"
	"spring-go.com/example/lig-example/dao"
	"spring-go.com/example/lig-example/interceptor"
	"spring-go.com/example/lig-example/main/support"
	"spring-go.com/example/lig-example/service"
	"spring-go.com/lig/lig"
)

func main() {
	engine, _ := support.GetDatasource()
	defer engine.Close()
	app := lig.NewMVC(log4g.LevelTrace)
	app.UseEngine(engine)

	userController := &controller.UserController{}
	zyService := &service.ZyService{}
	zyDao := &dao.ZyEntity{}

	logger := log4g.NewLogger(log4g.LevelTrace, "APP...", "spring-go.com")
	app.Install(logger,
		userController,
		zyService,
		zyDao)

	app.Interceptors(&interceptor.AdminInterceptor{})

	app.BuildResources()
	app.Publish(":8080")
}
