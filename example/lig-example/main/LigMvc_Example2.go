package main

import (
	_ "github.com/go-sql-driver/mysql"
	"spring-go.com/common/log4g"
	"spring-go.com/example/lig-example/main/support"
	"spring-go.com/lig/lig"
)

func main() {
	engine, _ := support.GetDatasource()
	defer engine.Close()

	app := lig.NewMVC(log4g.LevelTrace)

	app.UseEngine(engine)

	app.Install((&support.Factory{}).GetBeans())

	//app.Interceptors(&interceptor.AdminInterceptor{})

	app.BuildResources()
	app.Publish(":8080")
}
