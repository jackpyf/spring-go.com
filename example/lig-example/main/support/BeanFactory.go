package support

import (
	"spring-go.com/common/log4g"
	"spring-go.com/example/lig-example/controller"
	"spring-go.com/example/lig-example/dao"
	"spring-go.com/example/lig-example/service"
)

type Factory struct {
	beans []interface{}
}

func (this *Factory) init() {
	this.beans = append(this.beans, log4g.NewLogger(log4g.LevelDebug, "APP...", "spring-go.com"))        //日志打印
	this.beans = append(this.beans, &controller.UserController{}, &service.ZyService{}, &dao.ZyEntity{}) //Controller，Service，Dao对应一条线，具体业务。。。
	this.beans = append(this.beans, &controller.ZyController{}, &service.ZyService{}, &dao.ZyEntity{})   //Controller，Service，Dao对应一条线，具体业务
}
func (this *Factory) GetBeans() []interface{} {
	this.init()
	return this.beans
}
