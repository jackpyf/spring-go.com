package support

import (
	"fmt"
	"github.com/go-xorm/core"
	"github.com/go-xorm/xorm"
)

func GetDatasource() (engine *xorm.Engine, err error) {
	engine, err = xorm.NewEngine("mysql", "bzym:bzymCDE#4rfv@tcp(139.159.246.49:3306)/gl?charset=utf8") // dbname是taoge
	if err != nil {
		fmt.Println(err)
		return
	}

	engine.ShowSQL(true) // 显示SQL的执行, 便于调试分析
	engine.SetTableMapper(core.SnakeMapper{})
	engine.SetMaxIdleConns(10)
	engine.SetMaxOpenConns(100)
	return
}
