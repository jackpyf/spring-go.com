package support

import (
	"github.com/Shopify/sarama"
	"spring-go.com/common/middleware/kafka/single"
)

func GetProducer() *single.Producer {
	kafKaConfig := single.KafKaConfig{Topic: "test", KafkaServer: "192.168.1.20:9092", Partition: 0, RequireAcks: sarama.WaitForAll}
	if producer, err := single.NewKafkaProducer(kafKaConfig); err != nil {
		panic(err)
	} else {
		return &producer
	}
}

func GetConsumer() *single.Consumer {
	kafKaConfig := single.KafKaConfig{Topic: "test", KafkaServer: "192.168.1.20:9092", Partition: 0}
	if consumer, err := single.NewKafkaConsumer(kafKaConfig); err != nil {
		panic(err)
	} else {
		return &consumer
	}
}
