package support

import (
	"github.com/garyburd/redigo/redis"
	"os"
	"spring-go.com/common/log4g"
)

func GetRedisPool() *redis.Pool {
	pool := &redis.Pool{
		MaxIdle:     16,
		MaxActive:   0,
		IdleTimeout: 300,
		Dial: func() (redis.Conn, error) {
			if conn, err := redis.Dial("tcp", "192.168.1.111:6379"); err != nil {
				log4g.ERROR("redis is not active")
				os.Exit(0)
			} else {
				return conn, nil
			}
			return nil, nil
		},
	}
	return pool
}
