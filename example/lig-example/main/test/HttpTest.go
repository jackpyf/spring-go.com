package main

import (
	"fmt"
	"spring-go.com/common/utils"
	"strconv"
	"strings"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	for i := 0; i < 100; i++ {
		wg.Add(i)
		go func() { Test() }()
	}
	wg.Wait()
	fmt.Println("main exist")
}

func Test() {
	var MAX = 1000
	for i := 0; i < MAX; i++ {
		address := "http://127.0.0.1:8080/zy/Test?a=" + strconv.Itoa(i)
		output := utils.HttpGet(address)
		output = strings.Replace(output, "\n", "", -1)
		tmp, _ := strconv.Atoi(output)
		if tmp == i {
			//fmt.Println("success")
		} else {
			fmt.Println("failed", tmp, i)
		}
	}

}
