package service

import (
	"spring-go.com/common/log4g"
	"spring-go.com/common/middleware/kafka/single"
	"spring-go.com/lig/constant"
)

type KafkaService struct {
	Logger *log4g.LocalLogger `autowired`
}

func (this *KafkaService) Runner(kafka *single.KafkaMessage) {
	this.Logger.INFO("Kafka Service msg offset: ", kafka.Message.Offset, " partition: ", kafka.Message.Partition, " Timestamp: ", kafka.Message.Timestamp.Format(constant.TIME_FORMAT), " value: ", string(kafka.Message.Value))
}
