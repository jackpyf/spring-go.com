package service

import (
	"spring-go.com/example/lig-example/dao"
)

type ZyService struct {
	ZyDao *dao.ZyEntity `autowired`
}

func (this *ZyService) BegainTransaction() {
}

func (this *ZyService) EndTransaction() {

}

func (this *ZyService) AddVersion(zy dao.Zy) (flag int64) {
	flag, _ = (*this.ZyDao).Insert(zy)
	return
}

func (this *ZyService) GetVersion(id int64) (zy dao.Zy) {
	zy = (*this.ZyDao).Get(id)
	return
}
