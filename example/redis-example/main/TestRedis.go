package main

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
)

var pool *redis.Pool

func init() {
	pool = &redis.Pool{
		MaxIdle:     16,
		MaxActive:   0,
		IdleTimeout: 300,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", "192.168.1.111:6379") //Inet&BluePay126
		},
	}

}

func main() {
	c := pool.Get()
	//c.Do("AUTH", "Inet&BluePay126")
	defer c.Close()

	_, err := c.Do("set", "abc", 100)
	if err != nil {
		fmt.Println("c.Do err ", err)
		return
	}

	r, err := redis.Int(c.Do("get", "abc"))
	if err != nil {
		fmt.Println("get abc failed", err)
		return
	}
	fmt.Println(r)
	fmt.Printf("r type is %T", r)
	pool.Close()
}
