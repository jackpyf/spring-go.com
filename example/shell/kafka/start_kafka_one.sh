#!/usr/bin/env bash

machine_ip=192.168.1.20

zk_ip=192.168.1.10

docker rm -f kafka
#--link zk-node1:zookeeper \
docker run -d --name kafka   \
--net docker-net --ip $machine_ip \
--env KAFKA_BROKER_ID=100 \
--env HOST_IP=$machine_ip \
--env KAFKA_ZOOKEEPER_CONNECT=$zk_ip:2181 \
--env KAFKA_ADVERTISED_HOST_NAME=$machine_ip \
--env KAFKA_ADVERTISED_PORT=9092 \
--restart=always \
wurstmeister/kafka

