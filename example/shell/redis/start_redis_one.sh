#!/usr/bin/env bash
# docker run -p 6379:6379 -v $PWD/data:/data  -d redis:3.2 redis-server --appendonly yes
docker rm -f redis-server

machine_ip_1=192.168.1.111

docker run --name redis-server --net docker-net --ip $machine_ip_1 -d redis:3.2 redis-server --appendonly yes