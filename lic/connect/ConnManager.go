package connect

import (
	"errors"
	"spring-go.com/lic/constant"
	"sync"
)

type ConnManager struct {
	conns map[constant.ConnType]IConnect
	lock  sync.RWMutex
}

func NewConnManager() *ConnManager {
	return &ConnManager{conns: make(map[constant.ConnType]IConnect)}
}

func (this *ConnManager) Add(conn IConnect) {
	this.lock.Lock()
	defer this.lock.Unlock()
	this.conns[conn.GetConnId()] = conn
}

func (this *ConnManager) Remove(conn IConnect) {
	this.lock.Lock()
	defer this.lock.Unlock()
	delete(this.conns, conn.GetConnId())

}
func (this *ConnManager) Find(id constant.ConnType) (IConnect, error) {
	this.lock.RLock()
	defer this.lock.RUnlock()

	if conn, ok := this.conns[id]; ok {
		return conn, nil
	} else {
		return nil, errors.New("connection not found")
	}
}
func (this *ConnManager) Size() int {
	return len(this.conns)
}
func (this *ConnManager) Empty() {
	this.lock.Lock()
	defer this.lock.Unlock()

	for id, conn := range this.conns {
		conn.Stop()
		delete(this.conns, id)
	}
}
