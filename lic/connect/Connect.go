package connect

import (
	"errors"
	"fmt"
	"io"
	"net"
	"reflect"
	"spring-go.com/common/log4g"
	"spring-go.com/common/utils"
	"spring-go.com/lic/constant"
	"spring-go.com/lic/message"
	"spring-go.com/lic/pack"
	"sync"
)

type Connect struct {
	server      interface{}
	conn        *net.TCPConn
	connId      constant.ConnType
	msgChan     chan []byte
	connManager *ConnManager
	lock        sync.RWMutex
}

func NewConn(server interface{}, connMgr *ConnManager, conn *net.TCPConn, connId constant.ConnType) *Connect {
	c := &Connect{
		server:      server,
		conn:        conn,
		connId:      connId,
		msgChan:     make(chan []byte, 10),
		connManager: connMgr,
	}
	connMgr.Add(c)
	return c
}

func (this *Connect) GetConnId() constant.ConnType {
	return this.connId
}

func (this *Connect) GetServer() interface{} {
	return this.server
}

func (this *Connect) Stop() {
	log4g.ERROR(fmt.Sprint(this.conn.RemoteAddr(), " Connect Closed"))
	close(this.msgChan)
	this.conn.Close()
	this.connManager.Remove(this)
	go this.CallOnClose()
}

func (this *Connect) CallOnClose() {
	this.MethodOnConnect("CallOnClose")
}

func (this *Connect) SendMsg(msgId int32, data []byte, buff bool) error {
	this.lock.Lock()
	defer this.lock.Unlock()
	dp := pack.NewPack()
	msg, err := dp.Pack(message.NewMessage(msgId, data))
	if err != nil {
		log4g.ERROR("Pack error msg id = ", msgId)
		return errors.New("Pack error msg ")
	}

	//写回客户端
	this.msgChan <- msg

	return nil
}

func (this *Connect) Start() {
	go this.callOnStart()
	go this.startReader()
	go this.startWriter()

}

func (this *Connect) callOnStart() {
	this.MethodOnConnect("CallOnStart")
}

func (this *Connect) startReader() {
	defer this.Stop()
	for {
		p := pack.NewPack()
		packData := make([]byte, p.PackLen())
		if _, err := io.ReadFull(this.conn, packData); err != nil {
			log4g.ERROR("Message Head Error ", err)
			break
		}
		up := pack.NewUnPack()
		message, err := up.UnPack(packData)
		if err != nil {
			log4g.ERROR("UnPack Error ", err)
			break
		}
		var data []byte
		if message.GetMsgLen() > 0 {
			data = make([]byte, message.GetMsgLen())
			if _, err := io.ReadFull(this.conn, data); err != nil {
				log4g.ERROR("Message Data Error ", err)
				break
			}
		}
		message.SetMsgData(data)
		request := Request{
			conn:      this,
			ServiceId: message.GetServiceId(),
			Message:   message,
		}
		//Put Request to Queue
		var iRequest IRequest
		iRequest = &request
		value := reflect.ValueOf(this.server)
		method := value.MethodByName("PutRequestIntoQueue")
		if method.Type().Kind() == reflect.Func {
			method.Call(utils.GetValues(iRequest))
		}
		/*
			var iRequest handle.IRequest
			iRequest = &request
			value := reflect.ValueOf(this.server)
			method := value.MethodByName("ExecuteService")
			if method.Type().Kind() == reflect.Func {
				method.Call(tools.GetValues(iRequest))
			}*/
		log4g.INFO("RECV:[" + fmt.Sprint(request.Message.GetServiceId()) + "," + string(request.Message.GetMsgData()) + "]")
	}

}
func (this *Connect) startWriter() {
	for {
		select {
		case data, ok := <-this.msgChan:
			if ok {
				//有数据要写给客户端
				if _, err := this.conn.Write(data); err != nil {
					log4g.ERROR("Send Buff Data error:, ", err, " Conn Writer exit")
					return
				}
			} else {
				break
				log4g.ERROR("msgBuffChan is Closed")
			}
		}
	}
}
