package connect

import "spring-go.com/lic/constant"

type IConnManager interface {
	Add(conn IConnect)
	Remove(conn IConnect)
	Find(id constant.ConnType)
	Size() int
	Empty()
}
