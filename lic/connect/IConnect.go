package connect

import "spring-go.com/lic/constant"

type IConnect interface {
	Start()
	Stop()
	SendMsg(msgId int32, data []byte, buff bool) error
	GetConnId() constant.ConnType
	GetServer() interface{}
}
