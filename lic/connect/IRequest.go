package connect

import "spring-go.com/lic/message"

type IRequest interface {
	GetServiceId() int32
	SendMsg(msgId int32, bytes []byte, b bool) error
	GetConn() IConnect
	GetMessage() message.IMessage
}
