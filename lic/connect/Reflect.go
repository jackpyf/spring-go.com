package connect

import (
	"reflect"
	"spring-go.com/common/utils"
)

func (this *Connect) MethodOnConnect(methodName string) {
	value := reflect.ValueOf(this.GetServer())
	method := value.MethodByName(methodName)
	if method.Type().Kind() == reflect.Func {
		method.Call(utils.GetValues(this))
	}
}
