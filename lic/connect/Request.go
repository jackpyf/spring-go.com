package connect

import (
	"spring-go.com/lic/message"
)

type Request struct {
	conn      IConnect
	ServiceId int32
	Message   message.IMessage //客户端请求的数据
}

func (this *Request) GetServiceId() int32 {
	return this.ServiceId
}

func (this *Request) SendMsg(msgId int32, bytes []byte, b bool) error {
	err := this.conn.SendMsg(msgId, bytes, true)
	return err
}
func (this *Request) GetConn() IConnect {
	return this.conn
}

func (this *Request) GetMessage() message.IMessage {
	return this.Message
}
