package constant

type ConnType int64

//type MsgType int64
type LenType int64

const MAX_CONN_SIZE = 4096
const MAX_MESSAGE_SIZE = 4096
const MAX_REQUEST_QUEUE_SIZE = 100
const MAX_REQUEST_POOL_SIZE = 100
