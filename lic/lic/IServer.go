package lic

import (
	"spring-go.com/lic/connect"
	"spring-go.com/lic/service"
)

type IServer interface {
	Start()
	Stop()
	Serve()
	OnStart(func(connect connect.IConnect))
	OnClose(func(connect connect.IConnect))
	CallOnStart(connect connect.IConnect)
	CallOnClose(connect connect.IConnect)
	Service(router service.IService)
	PutRequestIntoQueue(request connect.IRequest)
	//ExecuteService(request handle.IRequest)
}
