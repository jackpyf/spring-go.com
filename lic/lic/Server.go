package lic

import (
	"fmt"
	"net"
	"spring-go.com/common/log4g"
	"spring-go.com/common/utils"
	"spring-go.com/lic/connect"
	"spring-go.com/lic/constant"
	"spring-go.com/lic/service"
	"time"
)

type ServerInfo struct {
	Ip         string
	Port       int
	IpProtocol string
}
type Server struct {
	serverInfo     *ServerInfo
	ConnMgr        *connect.ConnManager
	OnConnStart    func(connect connect.IConnect)
	OnConnClose    func(connect connect.IConnect)
	serviceFactory *service.ServiceFactory
}

func NewLic(ip string, port int) IServer {
	if len(ip) < 1 {
		ip = "127.0.0.1"
	}
	if port <= 0 {
		log4g.CRITICAL("Port is Null")
		return nil
	}
	server := &Server{
		serverInfo:     &ServerInfo{Ip: ip, Port: port, IpProtocol: "tcp4"},
		ConnMgr:        connect.NewConnManager(),
		serviceFactory: service.NewServiceFactory(),
	}
	return server
}

func (this *Server) PutRequestIntoQueue(request connect.IRequest) {
	this.serviceFactory.PutRequestIntoQueue(request)
}

/*
func (this *Server) ExecuteService(request handle.IRequest) {
	this.serviceFactory.Execute(request)
}*/
func (this *Server) CallOnStart(conn connect.IConnect) {
	if this.OnConnStart != nil {
		this.OnConnStart(conn)
	}
}

func (this *Server) CallOnClose(conn connect.IConnect) {
	if this.OnConnClose != nil {
		this.OnConnClose(conn)
	}
}

func (this *Server) OnStart(callback func(connect.IConnect)) {
	this.OnConnStart = callback
}

func (this *Server) OnClose(callback func(connect.IConnect)) {
	this.OnConnClose = callback
}

func (this *Server) Service(service service.IService) {
	this.serviceFactory.Add(service)
}

func (this *Server) Stop() {
	this.ConnMgr.Empty()
}

func (this *Server) Serve() {
	if len(this.serviceFactory.GetServices()) == 0 {
		log4g.ERROR("No service installed,Please use X.Service(&PingService{}) to install")
	} else {
		go this.Start()
		go this.serviceFactory.InitServiceQueue()
		go this.reports()
		select {}
	}
}
func (this *Server) reports() {
	for {
		log4g.INFO(fmt.Sprint("Connection Size:", this.ConnMgr.Size()))
		time.Sleep(1 * time.Minute)
	}
}
func (this *Server) Start() {
	address, err := net.ResolveTCPAddr(this.serverInfo.IpProtocol, fmt.Sprintf("%s:%d", this.serverInfo.Ip, this.serverInfo.Port))
	if err != nil {
		log4g.ERROR("Tcp addr err: ", err)
		return
	}

	listenner, err := net.ListenTCP(this.serverInfo.IpProtocol, address)
	if err != nil {
		log4g.ERROR("Listen err: ", err)
		return
	}
	log4g.INFO("start LIC server on " + fmt.Sprintf("%s:%d", this.serverInfo.Ip, this.serverInfo.Port))

	idWorker := &utils.IdWorker{}

	for {
		conn, err := listenner.AcceptTCP()
		if err != nil {
			log4g.ERROR("Accept err ", err)
			continue
		}
		if this.ConnMgr.Size() >= constant.MAX_CONN_SIZE {
			log4g.ERROR("No enough connection can be support!")
			conn.Close()
			continue
		}

		snowId, _ := idWorker.NextId()
		conn.SetKeepAlive(true)
		tcpConn := connect.NewConn(this, this.ConnMgr, conn, constant.ConnType(snowId))

		go tcpConn.Start()
	}

}
