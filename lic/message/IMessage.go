package message

import "spring-go.com/lic/constant"

type IMessage interface {
	GetMsgLen() constant.LenType
	GetServiceId() int32
	GetMsgData() []byte
	SetMsgData(data []byte)
}
