package message

import (
	"spring-go.com/lic/constant"
)

type Message struct {
	ServiceId int32
	MsgData   []byte
	MsgLen    constant.LenType
}

func (this *Message) GetServiceId() int32 {
	return this.ServiceId
}

func (this *Message) GetMsgLen() constant.LenType {
	return this.MsgLen
}
func (this *Message) GetMsgData() []byte {
	return this.MsgData
}

func (this *Message) SetMsgData(data []byte) {
	this.MsgData = data
}

func NewMessage(serviceId int32, msgData []byte) *Message {
	return &Message{
		ServiceId: serviceId,
		MsgData:   msgData,
		MsgLen:    constant.LenType(len(msgData)),
	}
}
