package pack

import (
	"spring-go.com/lic/constant"
	"spring-go.com/lic/message"
)

type IPack interface {
	PackLen() constant.LenType
	Pack(msg message.IMessage) ([]byte, error)
}
