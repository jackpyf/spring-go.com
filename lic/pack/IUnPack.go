package pack

import "spring-go.com/lic/message"

type IUnPack interface {
	Unpack([]byte) (message.IMessage, error)
}
