package pack

import (
	"bytes"
	"encoding/binary"
	"spring-go.com/lic/constant"
	"spring-go.com/lic/message"
)

type Pack struct {
}

func NewPack() *Pack {
	return &Pack{}
}
func (this *Pack) PackLen() constant.LenType {
	return 12
}
func (this *Pack) Pack(msg message.IMessage) ([]byte, error) {
	buff := bytes.NewBuffer([]byte{})
	if err := binary.Write(buff, binary.LittleEndian, msg.GetMsgLen()); err != nil {
		return nil, err
	}
	if err := binary.Write(buff, binary.LittleEndian, msg.GetServiceId()); err != nil {
		return nil, err
	}
	if err := binary.Write(buff, binary.LittleEndian, msg.GetMsgData()); err != nil {
		return nil, err
	}
	return buff.Bytes(), nil
}
