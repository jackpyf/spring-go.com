package pack

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"spring-go.com/lic/constant"
	"spring-go.com/lic/message"
)

type UnPack struct {
}

func NewUnPack() *UnPack {
	return &UnPack{}
}
func (this *UnPack) Len() constant.LenType {
	return 12
}
func (this *UnPack) UnPack(data []byte) (message.IMessage, error) {
	buff := bytes.NewReader(data)
	msg := &message.Message{}
	if err := binary.Read(buff, binary.LittleEndian, &msg.MsgLen); err != nil {
		return nil, err
	}
	if err := binary.Read(buff, binary.LittleEndian, &msg.ServiceId); err != nil {
		return nil, err
	}
	if constant.MAX_MESSAGE_SIZE < msg.MsgLen {
		return nil, errors.New(fmt.Sprint("Message Length = ", msg.MsgLen, ">=", constant.MAX_MESSAGE_SIZE))
	}
	return msg, nil
}
