package service

import (
	"spring-go.com/lic/connect"
)

type IService interface {
	Process(request connect.IRequest)
}
