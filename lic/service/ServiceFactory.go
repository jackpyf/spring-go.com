package service

import (
	"fmt"
	"spring-go.com/common/log4g"
	"spring-go.com/lic/connect"
	"spring-go.com/lic/constant"
)

type ServiceFactory struct {
	services     map[int32]IService
	requestQueue []chan connect.IRequest
}

func NewServiceFactory() *ServiceFactory {
	return &ServiceFactory{services: make(map[int32]IService), requestQueue: make([]chan connect.IRequest, constant.MAX_REQUEST_QUEUE_SIZE)}
}
func (this *ServiceFactory) GetServices() map[int32]IService {
	return this.services
}

func (this *ServiceFactory) Add(service IService) {
	//tmp:=strings.Split(reflect.TypeOf(service).String(),".")
	//serviceName:=string(tmp[len(tmp)-1])
	serviceId := int32(len(this.services))
	this.services[serviceId] = service
}

func (this *ServiceFactory) InitServiceQueue() {
	for i := 0; i < constant.MAX_REQUEST_QUEUE_SIZE; i++ {
		this.requestQueue[i] = make(chan connect.IRequest, constant.MAX_REQUEST_POOL_SIZE)
		go this.startService(i, this.requestQueue[i])
	}
	log4g.INFO("Queue Size:", constant.MAX_REQUEST_QUEUE_SIZE)
}

func (this *ServiceFactory) startService(serviceId int, requests chan connect.IRequest) {
	for {
		select {
		case request := <-requests:
			this.Execute(request)
		}
	}
}

func (this *ServiceFactory) Execute(request connect.IRequest) {
	serviceId := request.GetServiceId()
	if service, ok := this.services[serviceId]; ok {
		service.Process(request)
	}
}

func (this *ServiceFactory) PutRequestIntoQueue(request connect.IRequest) {
	queueId := int64(request.GetConn().GetConnId()) % int64(len(this.requestQueue))
	log4g.INFO(fmt.Sprint("ConnId=", request.GetConn().GetConnId(), " ServiceId=", request.GetServiceId(), " QueueId=", queueId))
	this.requestQueue[queueId] <- request
}
