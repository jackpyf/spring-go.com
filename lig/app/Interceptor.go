package app

type Interceptor interface {
	Interceptor() (bool, string)
}
