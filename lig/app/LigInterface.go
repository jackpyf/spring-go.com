package app

import "spring-go.com/lig/batch"

type Mvc interface {
	LigLogo()
	UseEngine(engine interface{})
	Controllers(controllers ...Controller)
	Interceptors(interceptors ...Interceptor)
	Install(beans ...interface{})
	Publish(addr string) error
	BuildResources()
	Runback(runner batch.Runner)
}
