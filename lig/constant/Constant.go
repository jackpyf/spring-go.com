package constant

const (
	TIME_FORMAT      string = "2006-01-02 03:04:05.000"
	DATASOURCE              = "datasource"
	BEAN_TAG                = "bean"
	SET_DAO                 = "SetDao"
	AUTOWIRED               = "autowired"
	INTERCEPTOR_SIZE        = 10
	INTERCEPTOR             = "Interceptor"
)
