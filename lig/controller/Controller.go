package controller

import (
	"reflect"
	"spring-go.com/common/log4g"
	"spring-go.com/common/utils"
	"spring-go.com/lig/app"
	"spring-go.com/lig/router"
)

type Controller struct {
	Logger *log4g.LocalLogger
}

func (this *Controller) PrepareController(router *router.Router, inf app.Controller) {
	//遍历路由器的方法，并将其存入控制器映射变量中
	this.installControllers(router, inf)
	this.installMethods(router, inf)
	this.installFeilds(router, inf)
}

func (this *Controller) installControllers(router *router.Router, inf app.Controller) {
	c := utils.GetControllerName(reflect.TypeOf(inf).String())
	v := reflect.ValueOf(inf)
	for i := 0; i < v.NumMethod(); i++ {
		if router.GetController(c) == nil {
			router.SetController(c, inf)
		}
	}
}
func (this *Controller) installMethods(router *router.Router, controller app.Controller) {
	c := utils.GetControllerName(reflect.TypeOf(controller).String())
	v := reflect.ValueOf(controller)
	t := reflect.TypeOf(controller)
	for i := 0; i < v.NumMethod(); i++ {
		m := t.Method(i).Name
		if router.GetMethod(utils.GetBeanKey(c, m)) == nil {
			router.SetMethod(utils.GetBeanKey(c, m), m)
			this.Logger.INFO("[/" + c + "/" + m + "] installed onto " + reflect.TypeOf(controller).String())
		}
	}

}
func (this *Controller) installFeilds(router *router.Router, controller app.Controller) {
	c := utils.GetControllerName(reflect.TypeOf(controller).String())
	fieldsName := utils.GetFieldName(controller)
	for index := range fieldsName {
		if utils.IsLowerStart(fieldsName[index].Name) {
			field := fieldsName[index]
			m := utils.ConvertToSetMethod(field.Name)
			c_m := utils.GetBeanKey(c, m)
			if router.GetMethod(c_m) != nil {
				router.SetField(utils.GetBeanKey(c, field.Name), field)
			}
			//else {
			//	this.Logger.WARN("[" + field.Name + "] no " + m + " method installed onto " + reflect.TypeOf(controller).String())
			//}
		}
	}
}
