package handle

import (
	"net/http"
	"spring-go.com/common/utils"
	"spring-go.com/lig/router"
)

/**
 *  URL过滤器获取到controller和method并且获取相应的对象进行处理
 */
func (this *HttpHandle) DoFilters(params map[string][]string, router *router.Router, r *http.Request, output *string) (errs error, statusCode int) {
	if c, m, err := this.checkApi(r); err == nil {
		if method, err, code := this.validatedMethod(params, router, c, m); err == nil {
			opt := method.Call(nil)
			*output = opt[0].String()
			*output = utils.ConvertToString(opt[0])
			//fmt.Println(opt[0].String())
		} else {
			errs = err
			statusCode = code
			this.logger.DEBUG(err.Error())
		}
	} else {
		this.logger.DEBUG(err.Error())
	}
	return
}
