package handle

import "net/http"

func (this *HttpHandle) initialHeaders(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") == "" {
		r.Header.Set("Content-Type", "application/json;charset=UTF-8")
	}
	w.Header().Set("Access-Control-Allow-Origin", "*")
}
