package handle

import (
	"encoding/json"
	"fmt"
	"net/http"
	"spring-go.com/common/log4g"
	"spring-go.com/lig/result"
	"spring-go.com/lig/router"
)

type HttpHandle struct {
	logger *log4g.LocalLogger
	router *router.Router
}

func NewHandle(logger *log4g.LocalLogger, router *router.Router) *HttpHandle {
	return &HttpHandle{logger, router}
}

//实现Handler接口
func (this *HttpHandle) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	type data struct {
		Data string `json:"data"`
		A    int    `json:"a"`
	}

	var (
		output string
		params = map[string][]string{}
	)
	this.initialHeaders(w, r)
	this.DoLogger(r, params)
	flag, value := this.DoInterceptors(w, r)
	if flag {
		if err, statusCode := this.DoFilters(params, this.router, r, &output); err != nil {
			w.WriteHeader(statusCode)
			output = fmt.Sprintf("%s", err)
			responseBody := &result.ResponseBody{}
			responseBody.Failure(output)
			returnData, _ := json.Marshal(responseBody)
			output = string(returnData)
		}
	} else {
		statusCode := 401
		w.WriteHeader(statusCode)
		output = value
		responseBody := &result.ResponseBody{}
		responseBody.Failure(output)
		returnData, _ := json.Marshal(responseBody)
		output = string(returnData)
	}
	size := len(output)
	if size > 0 {
		this.logger.TRACE(output)
	}
	this.Output(w, output)
}
