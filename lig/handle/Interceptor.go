package handle

import (
	"net/http"
	"reflect"
	"spring-go.com/lig/constant"
)

func (this *HttpHandle) DoInterceptors(w http.ResponseWriter, r *http.Request) (bool, string) {
	for _, v := range this.router.GetInterceptors() {
		interceptor := reflect.ValueOf(v).Elem()
		ww := interceptor.FieldByName("W")
		if ww.Kind() != reflect.Invalid {
			interceptor.FieldByName("W").Set(reflect.ValueOf(w))
		}
		rr := interceptor.FieldByName("R")
		if rr.Kind() != reflect.Invalid {
			interceptor.FieldByName("R").Set(reflect.ValueOf(r))
		}
		method := reflect.ValueOf(v).MethodByName(constant.INTERCEPTOR)
		if method.Kind() == reflect.Func {
			result := method.Call(nil)
			if result[0].Bool() == false {
				var value = ""
				if len(result) > 1 {
					value = result[1].String()
				}
				return false, value
			}
		}
	}
	return true, ""
}
