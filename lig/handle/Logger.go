package handle

import (
	"net/http"
	"spring-go.com/common/utils"
)

func (this *HttpHandle) DoLogger(r *http.Request, params map[string][]string) {
	msgStr := r.RemoteAddr + " " + r.RequestURI
	this.logger.TRACE(msgStr)
	r.ParseForm()
	paramsJson := "{"
	for form := range r.Form {
		values := "["
		var arrs []string
		for val := range r.Form[form] {
			values = values + r.Form[form][val] + ","
			arrs = append(arrs, r.Form[form][val])
		}
		params[form] = arrs
		values = utils.SubString(values, 0, len(values)-1)
		values = values + "]"
		paramsJson = paramsJson + form + "=" + values + ","
	}
	if len(paramsJson) > 1 {
		paramsJson = utils.SubString(paramsJson, 0, len(paramsJson)-1)
	}
	paramsJson = paramsJson + "}"
	this.logger.TRACE("params=" + paramsJson)
	return
}
