package handle

import (
	"reflect"
	"spring-go.com/common/utils"
	"spring-go.com/lig/router"
)

func (this *HttpHandle) injectValues(router *router.Router, controller *interface{}, params map[string][]string) {
	c := utils.GetControllerName(reflect.TypeOf(*controller).String())
	for k, v := range params {
		f := k
		field := router.GetField(utils.GetBeanKey(c, f)) //有field就有对应的Set method与之对应
		if field.Name != "" {
			m := utils.ConvertToSetMethod(field.Name)
			value := reflect.ValueOf(*controller)
			method := value.MethodByName(m)
			if utils.IsAvailableType(field.Type) {
				method.Call(this.getFieldValue(field, v, true)) //参数注入进去
			} else if utils.IsAvailableArrType(field.Type) {
				method.Call(this.getFieldValue(field, v, false)) //参数注入进去
			}
		} else {
			this.logger.DEBUG("******Field:" + k + " not used")
		}
	}
}
func (this *HttpHandle) getFieldValue(field reflect.StructField, v []string, single bool) (value []reflect.Value) {
	if single {
		//处理数组传入的情况
		if len(v) >= 1 {
			vv, _ := utils.ValueConvert(field.Type.String(), v[0])
			value = utils.GetValues(vv)
		}
	} else {
		value, _ = utils.ValuesConvert(field.Type.String(), v)
	}
	return
}
