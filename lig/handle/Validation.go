package handle

import (
	"errors"
	"net/http"
	"reflect"
	"spring-go.com/common/utils"
	"spring-go.com/lig/router"
)

func (this *HttpHandle) checkApi(r *http.Request) (c string, m string, err error) {
	URI := r.RequestURI
	c, m = utils.GetCAndMFromURI(URI)
	if len(c) == 0 || len(m) == 0 {
		err = errors.New("Not validated URI!" + r.RequestURI)
		return
	}
	return
}
func (this *HttpHandle) validatedMethod(params map[string][]string, router *router.Router, c, m string) (method reflect.Value, err error, statusCode int) {
	controller := utils.Clone(router.GetController(c))
	if controller == nil {
		err = errors.New("Controller:" + c + "Controller Not Installed")
		statusCode = 404
		return
	}
	this.injectValues(router, &controller, params)
	handlerValue := reflect.ValueOf(controller)
	method = handlerValue.MethodByName(m)
	if method.Kind() == reflect.Invalid {
		if utils.IsUpperStart(m) {
			err = errors.New("Method:" + m + " Not found in " + c + "Controller")
		} else {
			err = errors.New("Method:" + m + " Not found in " + c + "Controller or Not exported,Please change to Method:" + utils.ToUpperStart(m))
		}
		statusCode = 404
		return
	}
	return
}
