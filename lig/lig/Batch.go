package lig

import (
	"spring-go.com/lig/batch"
)

func (this *Mvc) Runback(runner batch.Runner) {
	/*
		var key = reflect.TypeOf(bean).String()
		v := this.router.GetBean(key)
		ee := reflect.ValueOf(v)
		method := ee.MethodByName("Exec")
		if method.Kind() == reflect.Func {
			go func() {
				method.Call(nil)
			}()
		}
	*/
	go func() {
		runner.Run()
	}()
}
