package lig

import (
	"os"
	"reflect"
	"spring-go.com/common/utils"
	"spring-go.com/lig/constant"
	"spring-go.com/lig/controller"
	"spring-go.com/lig/router"
)

func (this *Mvc) buildBeans(router *router.Router, bean interface{}) {
	var key = reflect.TypeOf(bean).String()
	if utils.IsController(key) {
		if router.GetController(key) == nil {
			controller := &controller.Controller{this.logger}
			controller.PrepareController(this.router, bean)
			this.logger.INFO(key + " installed success")
		} else {
			this.logger.TRACE(key + " already installed")
		}
	} else {
		if router.GetBean(key) == nil {
			router.SetBean(key, bean)
			this.logger.INFO(key + " installed success")
		} else {
			this.logger.TRACE(key + " already installed")
		}
	}
}

func (this *Mvc) injectBeans(router *router.Router) {
	for _, v := range router.GetBeans() {
		e := reflect.ValueOf(v).Elem()
		fieldsName := utils.GetFieldName(v)
		for _, val := range fieldsName {
			if string(val.Tag) == constant.AUTOWIRED {
				if utils.IsUpperStart(val.Name) {
					if bean := router.GetBean(val.Type.String()); bean != nil {
						e.FieldByName(val.Name).Set(reflect.ValueOf(bean))
					} else {
						this.logger.WARN("[" + val.Name + "] Not initialled onto " + reflect.TypeOf(v).String())
						panic("No " + val.Type.String() + " installed!")
					}
				} else {
					this.logger.ERROR("Please change " + val.Name + " to " + utils.ToUpperStart(val.Name) + " onto " + reflect.TypeOf(v).String())
					os.Exit(0)
				}
			}
		}
		ee := reflect.ValueOf(v)
		method := ee.MethodByName(constant.SET_DAO)
		if method.Kind() == reflect.Func {
			method.Call(utils.GetValues(router.GetDao()))
		}
	}
}
