package lig

import (
	"os"
	"reflect"
	"spring-go.com/common/utils"
	"spring-go.com/lig/app"
	"spring-go.com/lig/constant"
	"spring-go.com/lig/controller"
	"spring-go.com/lig/router"
)

func (this *Mvc) Controllers(controllers ...app.Controller) {
	controller := &controller.Controller{this.logger}
	for _, v := range controllers {
		controller.PrepareController(this.router, v)
	}
}

func (this *Mvc) injectControllers(router *router.Router) {
	for _, v := range router.GetControllers() {
		e := reflect.ValueOf(v).Elem()
		fieldsName := utils.GetFieldName(v)
		for _, val := range fieldsName {
			if string(val.Tag) == constant.AUTOWIRED {
				if utils.IsUpperStart(val.Name) {
					if bean := router.GetBean(val.Type.String()); bean != nil {
						e.FieldByName(val.Name).Set(reflect.ValueOf(bean))
					} else {
						if control := router.GetController(utils.GetControllerName(val.Type.String())); control != nil {
							e.FieldByName(val.Name).Set(reflect.ValueOf(control))
						} else {
							this.logger.WARN("[" + val.Name + "] Not initialled onto " + reflect.TypeOf(v).String())
						}
					}
				} else {
					this.logger.ERROR("Please change " + val.Name + " to " + utils.ToUpperStart(val.Name) + " onto " + reflect.TypeOf(v).String())
					os.Exit(0)
				}
			}
		}
	}
}
