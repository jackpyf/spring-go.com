package lig

import (
	"spring-go.com/lig/constant"
)

func (this *Mvc) UseEngine(engine interface{}) {
	this.router.SetBean(constant.DATASOURCE, engine)
}
