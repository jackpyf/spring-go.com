package lig

func (this *Mvc) Install(beans ...interface{}) {
	for _, v := range beans {
		switch v.(type) {
		case []interface{}:
			for _, elem := range v.([]interface{}) {
				this.buildBeans(this.router, elem)
			}
		case interface{}:
			this.buildBeans(this.router, v)
		}
	}
}
