package lig

import (
	"spring-go.com/common/log4g"
)

func (this *Mvc) UseLogger(logger *log4g.LocalLogger) {
	this.logger = logger
}
