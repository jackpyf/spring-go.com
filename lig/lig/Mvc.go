package lig

import (
	"net/http"
	"spring-go.com/common/log4g"
	"spring-go.com/lig/app"
	"spring-go.com/lig/handle"
	"spring-go.com/lig/router"
	"sync"
)

/**
  Gmvc结构体
*/
type Mvc struct {
	router       *router.Router                           //路由
	HandlerError func(http.ResponseWriter, *http.Request) //处理错误
	logger       *log4g.LocalLogger
	handle       *handle.HttpHandle
	mu           sync.Mutex
}

func NewMVC(logType int) app.Mvc {
	var mvc app.Mvc
	logger := log4g.NewLogger(logType, "SYSTEM", "spring-go.com")
	router := router.NewRouter(logger)
	handle := handle.NewHandle(logger, router)
	mvc = &Mvc{
		logger: logger,
		router: router,
		handle: handle,
	}
	mvc.LigLogo()
	return mvc
}
