package lig

import "spring-go.com/lig/server"

//启动一个服务器
func (this *Mvc) Publish(addr string) error {
	httpServer := server.NewHttpServer()
	this.logger.INFO("Server Started on " + addr)
	return httpServer.Start(addr, this.handle)
}

func (this *Mvc) prepareRouter() {
	this.injectBeans(this.router)
	this.injectControllers(this.router)
	this.injectInterceptors(this.router)
}

func (this *Mvc) BuildResources() {
	this.prepareRouter()
}
