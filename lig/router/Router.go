package router

import (
	"reflect"
	"spring-go.com/common/log4g"
	"spring-go.com/lig/app"
	"spring-go.com/lig/constant"
)

type Router struct {
	controllers  map[string]interface{}
	methods      map[string]interface{}
	fields       map[string]reflect.StructField
	beans        map[string]interface{}
	interceptors []app.Interceptor
	logger       *log4g.LocalLogger
}

func (this *Router) GetControllers() map[string]interface{} {
	return this.controllers
}
func (this *Router) GetFields() map[string]reflect.StructField {
	return this.fields
}
func (this *Router) GetBeans() map[string]interface{} {
	return this.beans
}
func NewRouter(logger *log4g.LocalLogger) *Router {
	router := Router{logger: logger, interceptors: make([]app.Interceptor, 0, constant.INTERCEPTOR_SIZE)}
	router.controllers = make(map[string]interface{})
	router.methods = make(map[string]interface{})
	router.fields = make(map[string]reflect.StructField)
	router.beans = make(map[string]interface{})
	return &router
}

func (this *Router) GetController(c string) interface{} {
	return this.controllers[c]
}
func (this *Router) SetController(k string, v interface{}) {
	this.controllers[k] = v
}
func (this *Router) GetInterceptors() []app.Interceptor {
	return this.interceptors
}
func (this *Router) AddInterceptor(v app.Interceptor) {
	this.interceptors = append(this.interceptors, v)
}
func (this *Router) GetMethod(c_m string) interface{} {
	return this.methods[c_m]
}
func (this *Router) SetMethod(c_m string, method interface{}) {
	this.methods[c_m] = method
}
func (this *Router) SetField(k string, v reflect.StructField) {
	this.fields[k] = v
}
func (this *Router) GetField(c_f string) reflect.StructField {
	return this.fields[c_f]
}

func (this *Router) GetBean(k string) interface{} {
	return this.beans[k]
}
func (this *Router) GetDao() interface{} {
	return this.beans[constant.DATASOURCE]
}
func (this *Router) SetBean(k string, v interface{}) {
	this.beans[k] = v
}
