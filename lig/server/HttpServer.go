package server

import (
	"net/http"
)

type HttpServer struct {
}

func NewHttpServer() *HttpServer {
	http := &HttpServer{}
	return http
}
func (this *HttpServer) Start(addr string, handle http.Handler) error {
	server := &http.Server{Addr: addr, Handler: handle}
	if err := server.ListenAndServe(); err != nil {
		panic(err)
		return err
	}
	return nil
}
