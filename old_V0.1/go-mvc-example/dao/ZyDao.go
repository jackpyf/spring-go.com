package dao

import (
	"crypto/md5"
	"fmt"
	"github.com/go-xorm/xorm"
	"reflect"
	"spring-go.com/go-mvc/log4g"
	"time"
)

type Zy struct {
	Id         int64     `json:"id" xorm:"pk autoincr comment('主键ID') BIGINT(20)"`
	Version    string    `json:"version" xorm:"comment('支持最大8个验证码') VARCHAR(8)"`
	Content    string    `json:"content" xorm:"comment('支持最大100个字符的短信') VARCHAR(100)"`
	CreateTime time.Time `json:"create_time" xorm:"not null default '0000-00-00 00:00:00' DATETIME"`
	UpdateTime time.Time `json:"update_time" xorm:"default '0000-00-00 00:00:00' DATETIME"`
}
type ZyEntity struct {
	Zy
	Logger *log4g.LocalLogger `autowired`
}

var (
	dao    *xorm.Engine
	getmd5 = md5.New()
	t      = time.Now()
)

func (this *ZyEntity) Insert(zy Zy) (affected int64, err error) {
	if affected, err = dao.Insert(zy); err != nil {
		fmt.Println(err)
	}
	return
}
func (this *ZyEntity) Get(id int64) Zy {
	var entity Zy
	entity.Id = id
	has, err := dao.Get(&entity)
	fmt.Println(err)
	fmt.Println("has=", has)
	return entity
}

func (this *ZyEntity) SetDao(v interface{}) {
	if engine, ok := (v).(*xorm.Engine); ok == true {
		dao = engine
	} else {
		fmt.Println("type: ", reflect.TypeOf(v))
		fmt.Println("value: ", reflect.ValueOf(v))
		panic("xorm.Engine is not found")
	}
}
