package dao

type ZyService interface {
	Insert(zy Zy) (affected int64, err error)
	Get(id int64) (affected int64, err error)
}
