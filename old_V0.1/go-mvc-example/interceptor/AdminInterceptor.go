package interceptor

import (
	"net/http"
	"spring-go.com/go-mvc/log4g"
)

type AdminInterceptor struct {
	W      http.ResponseWriter
	R      *http.Request
	Logger *log4g.LocalLogger `autowired`
}

func (this *AdminInterceptor) Interceptor() (bool, string) {
	this.Logger.DEBUG("Interceptor")
	return false, "Error"
}
