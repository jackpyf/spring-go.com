package main

import (
	_ "github.com/go-sql-driver/mysql"
	"spring-go.com/go-mvc-example/controller"
	"spring-go.com/go-mvc-example/dao"
	"spring-go.com/go-mvc-example/interceptor"
	"spring-go.com/go-mvc-example/main/support"
	"spring-go.com/go-mvc-example/service"
	"spring-go.com/go-mvc/context"
	"spring-go.com/go-mvc/log4g"
)

func main() {
	engine, _ := support.GetDatasource()
	defer engine.Close()
	app := context.NewMVC()
	app.UseDao(engine)

	userController := &controller.UserController{}
	zyService := &service.ZyService{}
	zyDao := &dao.ZyEntity{}
	logger := log4g.NewLogger("APP...")
	app.InstallSlice(logger,
		userController,
		zyService,
		zyDao)
	app.Interceptors(&interceptor.AdminInterceptor{})
	app.ListenAndServe(":8080")
}
