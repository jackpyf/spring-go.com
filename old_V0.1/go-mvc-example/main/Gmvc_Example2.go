package main

import (
	_ "github.com/go-sql-driver/mysql"
	"spring-go.com/go-mvc-example/interceptor"
	"spring-go.com/go-mvc-example/main/support"
	"spring-go.com/go-mvc/context"
)

func main() {
	engine, _ := support.GetDatasource()
	defer engine.Close()

	app := context.NewMVC()

	app.UseDao(engine)

	app.InstallArr((&support.Factory{}).GetBeans())

	app.Interceptors(&interceptor.AdminInterceptor{})

	app.ListenAndServe(":8080")
}
