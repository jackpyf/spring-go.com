package support

import (
	"spring-go.com/go-mvc-example/controller"
	"spring-go.com/go-mvc-example/dao"
	"spring-go.com/go-mvc-example/service"
	"spring-go.com/go-mvc/log4g"
)

type Factory struct {
	beans []interface{}
}

func (this *Factory) factory() {
	this.beans = append(this.beans, log4g.NewLogger("APP..."))                                           //日志打印
	this.beans = append(this.beans, &controller.UserController{}, &service.ZyService{}, &dao.ZyEntity{}) //Controller，Service，Dao对应一条线，具体业务。。。
	this.beans = append(this.beans, &controller.ZyController{}, &service.ZyService{}, &dao.ZyEntity{})   //Controller，Service，Dao对应一条线，具体业务
}
func (this *Factory) GetBeans() []interface{} {
	this.factory()
	return this.beans
}
