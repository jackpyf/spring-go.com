package app

type GoMvc interface {
	WelcomeLogo()
	UseDao(engine interface{})
	Controllers(controllers ...Controller)
	Interceptors(interceptors ...Interceptor)
	InstallArr(beans []interface{})
	InstallSlice(beans ...interface{})
	ListenAndServe(addr string) error
}
