package context

import (
	"net/http"
	"spring-go.com/go-mvc/app"
	"spring-go.com/go-mvc/handle"
	"spring-go.com/go-mvc/log4g"
	"spring-go.com/go-mvc/router"
	"sync"
)

/**
  Gmvc结构体
*/
type GoMvc struct {
	router       *router.Router                           //路由
	HandlerError func(http.ResponseWriter, *http.Request) //处理错误
	logger       *log4g.LocalLogger
	handle       *handle.HttpHandle
	mu           sync.Mutex
}

func NewMVC() app.GoMvc {
	var goMvc app.GoMvc
	logger := log4g.NewLogger("SYSTEM")
	router := router.NewRouter(logger)
	handle := handle.NewHandle(logger, router)
	goMvc = &GoMvc{
		logger: logger,
		router: router,
		handle: handle,
	}
	goMvc.WelcomeLogo()
	return goMvc
}
