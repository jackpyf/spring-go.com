package context

import (
	"reflect"
	"spring-go.com/go-mvc/constant"
	"spring-go.com/go-mvc/controller"
	"spring-go.com/go-mvc/router"
	"spring-go.com/go-mvc/utils"
)

func (this *GoMvc) buildBeans(router *router.Router, bean interface{}) {
	var key = reflect.TypeOf(bean).String()
	if utils.IsController(key) {
		if router.GetController(key) == nil {
			controller := &controller.Controller{this.logger}
			controller.PrepareController(this.router, bean)
		}
	} else {
		router.SetBean(key, bean)
		this.logger.INFO(key + " installed success")
	}
}

func (this *GoMvc) injectBeans(router *router.Router) {
	for _, v := range router.GetBeans() {
		e := reflect.ValueOf(v).Elem()
		fieldsName := utils.GetFieldName(v)
		for _, val := range fieldsName {
			if utils.IsUpperStart(val.Name) {
				if string(val.Tag) == constant.AUTOWIRED {
					if bean := router.GetBean(val.Type.String()); bean != nil {
						e.FieldByName(val.Name).Set(reflect.ValueOf(bean))
					} else {
						this.logger.WARN("[" + val.Name + "] Not initialled onto " + reflect.TypeOf(v).String())
						panic("No " + val.Type.String() + " installed!")
					}
				}
			}
		}
		ee := reflect.ValueOf(v)
		method := ee.MethodByName(constant.SET_DAO)
		if method.Kind() == reflect.Func {
			method.Call(utils.GetValues(router.GetDao()))
		}
	}
}
