package context

import (
	"spring-go.com/go-mvc/constant"
	"spring-go.com/go-mvc/log4g"
)

func (this *GoMvc) UseDao(engine interface{}) {
	this.router.SetBean(constant.DATASOURCE, engine)
}

func (this *GoMvc) UseLogger(logger *log4g.LocalLogger) {
	this.logger = logger
}
