package context

func (this *GoMvc) InstallArr(beans []interface{}) {
	for _, v := range beans {
		this.buildBeans(this.router, v)
	}
}

func (this *GoMvc) InstallSlice(beans ...interface{}) {
	for _, v := range beans {
		this.buildBeans(this.router, v)
	}
}
