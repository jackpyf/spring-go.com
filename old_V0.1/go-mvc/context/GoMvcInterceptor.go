package context

import (
	"reflect"
	"spring-go.com/go-mvc/app"
	"spring-go.com/go-mvc/constant"
	"spring-go.com/go-mvc/router"
	"spring-go.com/go-mvc/utils"
)

func (this *GoMvc) Interceptors(interceptors ...app.Interceptor) {
	for _, interceptor := range interceptors {
		this.router.AddInterceptor(interceptor)
	}
}

func (this *GoMvc) injectInterceptors(router *router.Router) {
	for _, v := range router.GetInterceptors() {
		e := reflect.ValueOf(v).Elem()
		fieldsName := utils.GetFieldName(v)
		for _, val := range fieldsName {
			if utils.IsUpperStart(val.Name) {
				if string(val.Tag) == constant.AUTOWIRED {
					if bean := router.GetBean(val.Type.String()); bean != nil {
						e.FieldByName(val.Name).Set(reflect.ValueOf(bean))
					} else {
						this.logger.WARN("[" + val.Name + "] Not initialled onto " + reflect.TypeOf(v).String())
					}
				}
			}
		}
	}
}
