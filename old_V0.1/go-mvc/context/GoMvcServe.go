package context

import "spring-go.com/go-mvc/server"

//启动一个服务器
func (this *GoMvc) ListenAndServe(addr string) error {
	httpServer := server.NewHttpServer()
	this.prepareRouter()
	this.logger.INFO("Server Started on " + addr)
	return httpServer.Start(addr, this.handle)
}

func (this *GoMvc) prepareRouter() {
	this.injectBeans(this.router)
	this.injectControllers(this.router)
	this.injectInterceptors(this.router)
}
