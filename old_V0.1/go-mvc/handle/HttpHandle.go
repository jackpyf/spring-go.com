package handle

import (
	"fmt"
	"net/http"
	"spring-go.com/go-mvc/log4g"
	"spring-go.com/go-mvc/router"
)

type HttpHandle struct {
	logger *log4g.LocalLogger
	router *router.Router
}

func NewHandle(logger *log4g.LocalLogger, router *router.Router) *HttpHandle {
	return &HttpHandle{logger, router}
}

//实现Handler接口
func (this *HttpHandle) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var (
		output string
		params = map[string][]string{}
	)
	this.initialHeaders(w, r)
	this.DoLogger(r, params)
	flag, value := this.DoInterceptors(w, r)
	fmt.Println(flag)
	if flag == false {
		statusCode := 401
		w.WriteHeader(statusCode)
		output = value
	} else if err, statusCode := this.DoFilters(params, this.router, r, &output); err != nil {
		w.WriteHeader(statusCode)
		output = fmt.Sprintf("%s", err)
	}
	this.Output(w, output)
}
