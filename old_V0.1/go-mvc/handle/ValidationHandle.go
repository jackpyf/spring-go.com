package handle

import (
	"errors"
	"net/http"
	"reflect"
	"spring-go.com/go-mvc/router"
	"spring-go.com/go-mvc/utils"
)

func (this *HttpHandle) checkApi(r *http.Request) (c string, m string, err error) {
	URI := r.RequestURI
	c, m = utils.GetCAndMFromURI(URI)
	if len(c) == 0 || len(m) == 0 {
		err = errors.New("Not validated URI!" + r.RequestURI)
		return
	}
	return
}
func (this *HttpHandle) validatedMethod(params map[string][]string, router *router.Router, c, m string) (method reflect.Value, err error, statusCode int) {
	controller := utils.Clone(router.GetController(c))
	if controller == nil {
		err = errors.New("Controller:" + c + "Controller Not Installed")
		statusCode = 404
		return
	}
	this.injectValues(router, &controller, params)
	handlerValue := reflect.ValueOf(controller)
	method = handlerValue.MethodByName(m)
	return
}
