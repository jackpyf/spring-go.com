package log4g

import (
	"errors"
	"os"
	"runtime"
	"sync"
	"time"
)

type consoleLogger struct {
	sync.Mutex
	Level    string `json:"level"`
	Colorful bool   `json:"color"`
	LogLevel int
}

type brush func(string) string

func newBrush(color string) brush {
	pre := "\033["
	reset := "\033[0m"
	return func(text string) string {
		return pre + color + "m" + text + reset
	}
}

//鉴于终端的通常使用习惯，一般白色和黑色字体是不可行的,所以30,37不可用，
var colors = []brush{
	newBrush("1;34"), // Critical           蓝色
	newBrush("1;31"), // Error              红色
	newBrush("1;33"), // Warn               黄色
	newBrush("1;36"), // Informational      天蓝色
	newBrush("1;33"), // Debug              棕色
	newBrush("1;32"), // Trace              绿色
}

func (c *consoleLogger) InitConfig(config interface{}) error {
	if cc, ok := config.(*consoleLogger); ok {
		if runtime.GOOS == "windows" {
			c.Colorful = false
		}
		if l, ok := LevelMap[cc.Level]; ok {
			c.LogLevel = l
		}
		return nil
	} else {
		err := errors.New("consoleLogger initialized failed!")
		return err
	}
}

func (c *consoleLogger) LogWrite(when time.Time, msgText interface{}, level int) error {
	if level > c.LogLevel {
		return nil
	}
	msg, ok := msgText.(string)
	if !ok {
		return nil
	}
	if c.Colorful {
		msg = colors[level](msg)
	}
	c.printlnConsole(when, msg)
	return nil
}

func (c *consoleLogger) printlnConsole(when time.Time, msg string) {
	c.Lock()
	defer c.Unlock()
	os.Stdout.Write(append([]byte(msg), '\n'))
}

func (c *consoleLogger) Destroy() {

}

/**
 * 初始化日志参数
 */
func init() {
	Register(AdapterConsole, &consoleLogger{
		LogLevel: LevelTrace,
		Colorful: runtime.GOOS != "windows",
	})
}
