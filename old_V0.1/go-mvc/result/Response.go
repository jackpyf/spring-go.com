package result

import (
	"spring-go.com/go-mvc/constant"
	"time"
)

const (
	ERROR int = 500
	FAIL  int = 503
	SUCC  int = 200
)

type ResponseBody struct {
	Data    interface{} `json:"data"`
	Status  int         `json:"status"`
	Message string      `json:"status"`
	Time    string      `json:"time"`
	Err     error       `json:"error"`
	Path    string      `json:"path"`
}

func (response *ResponseBody) Success(data interface{}) {
	when := time.Now()
	response.Time = when.Format(constant.TIME_FORMAT)
	response.Status = SUCC
	response.Data = data
}
func (response *ResponseBody) Failure(msg string) {
	when := time.Now()
	response.Time = when.Format(constant.TIME_FORMAT)
	response.Status = FAIL
	response.Message = msg
}
func (response *ResponseBody) Error(msg string, err error) {
	when := time.Now()
	response.Time = when.Format(constant.TIME_FORMAT)
	response.Status = ERROR
	response.Err = err
	response.Message = msg
}
