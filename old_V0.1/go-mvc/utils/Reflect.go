package utils

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
)

func Call(inf interface{}, params ...interface{}) ([]reflect.Value, error) {
	f := reflect.ValueOf(inf)
	if len(params) != f.Type().NumIn() {
		return nil, errors.New("the number of input params not match!")
	}

	in := make([]reflect.Value, len(params))
	for k, v := range params {
		in[k] = reflect.ValueOf(v)
	}
	return f.Call(in), nil
}

func GetValues(param ...interface{}) []reflect.Value {
	vals := make([]reflect.Value, 0, len(param))
	for i := range param {
		vals = append(vals, reflect.ValueOf(param[i]))
	}
	return vals
}
func GetFieldName(structName interface{}) []reflect.StructField {
	t := reflect.TypeOf(structName)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	if t.Kind() != reflect.Struct {
		fmt.Println("Check type error not Struct")
		return nil
	}
	fieldNum := t.NumField()
	result := make([]reflect.StructField, 0, fieldNum)
	for i := 0; i < fieldNum; i++ {
		result = append(result, t.Field(i))
	}
	return result
}

func GetMethods(structName interface{}) []string {
	t := reflect.TypeOf(structName)
	methodNum := t.NumMethod()
	result := make([]string, 0, methodNum)
	for i := 0; i < methodNum; i++ {
		result = append(result, t.Method(i).Name)
	}
	return result
}

//获取结构体中Tag的值，如果没有tag则返回字段值
func GetTagName(structName interface{}) []string {
	t := reflect.TypeOf(structName)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	if t.Kind() != reflect.Struct {
		fmt.Println("Check type error not Struct")
		return nil
	}
	fieldNum := t.NumField()
	result := make([]string, 0, fieldNum)
	for i := 0; i < fieldNum; i++ {
		tagName := t.Field(i).Name
		tags := strings.Split(string(t.Field(i).Tag), "\"")
		if len(tags) > 1 {
			tagName = tags[1]
		}
		result = append(result, tagName)
	}
	return result
}
